#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "tool_1.h"

//各种模拟实现string函数测试
int test_3(void)
{
	char arr1[20] = "1234567";
	char arr2[] = "567";
	
	printf("%s\n", (char*)my_memmove(arr1, arr1+2, 3));
	return 0;
}