#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define SWAP(SWAP_NUM) ((0xaaaaaaaa & (SWAP_NUM)) >> 1) + ((0x55555555 & (SWAP_NUM)) << 1)

int test_6(void)
{
	printf("%d", SWAP(1));
	return 0;
}