#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int cmp(void* e1, void* e2)
{
	return *((int*)e2) - *((int*)e1);
}

int test_2(void)
{
	int arr[] = { 0,2,5,8,0,1,8,5,3,9,1,2,4,6,7,9,4,7 };
	int length = sizeof(arr) / sizeof(arr[0]);

	qsort(arr, length, 4, cmp);	//排序
	
	int nums[2] = { 0 };
	int j = 0;
	int i = 0;
	while (i < length-1)	//判断相邻两个是否一样
	{
		if (arr[i] == arr[i + 1])
		{
			i += 2;
		}
		else
		{
			nums[j] = arr[i];
			i++;
			j++;
		}
	}
	printf("%d %d", nums[0], nums[1]);
	return 0;
}