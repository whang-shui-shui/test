#define _CRT_SECURE_NO_WARNINGS
#include <assert.h>
#include <string.h>
#include <math.h>

//模拟实现strncpy
char* my_strncpy(char* dest, const char* sour, int num)
{
	assert(dest != NULL, sour != NULL, num >= 0);

	char* ret = dest;
	while (num--)
	{
		if (sour != '\0')
		{
			*dest++ = *sour++;
		}
	}
	return ret;
}

//strncap模拟实现
char* my_strncat(char* dest, const char* sour, int num)
{
	assert(dest != NULL, sour != NULL, num >= 0);
	char* ret = dest;
	
	while (*dest != '\0')
	{
		dest++;
	}
	
	while (num--)
	{
		if (sour != '\0')
		{
			*dest++ = *sour++;
		}
	}
	*dest = '\0';

	return ret;
}

//atoi模拟实现
int my_atoi(const char* str)
{
	assert(str != NULL);
	int len = strlen(str);

	int ret = 0;

	int i = 0;
	for (i=len-1; i>=0; --i)
	{
		if ('0' > str[i] && str[i] > '9')
		{
			ret = -1;
			break;
		}
		ret += (int)(str[i] - '0') * (int)pow(10, len - i - 1);
	}
	return ret;
}

//strlen模拟实现
int my_strlen(const char* str)
{
	assert(str != NULL);
	int count = 0;
	while (*str != '\0')
	{
		str++;
		count++;
	}
	return count;
}

//strcpy模拟实现
char* my_strcpy(char* dest, const char* sour)
{
	assert(dest != NULL && sour != NULL);
	char* ret = dest;
	
	while (*dest++ = *sour++)
	{
		;
	}

	return ret;
}

//strcat模拟实现
char* my_strcat(char* dest, const char* sour)
{
	assert(dest != NULL && sour != NULL);
	char* ret = dest;

	while (*dest != '\0')
	{
		dest++;
	}

	while (*dest++ = *sour++)
	{
		;
	}

	return ret;
}

//strcmp模拟实现
int my_strcmp(const char* dest, const char* sour)
{
	assert(dest != NULL && sour != NULL);

	while (*dest == *sour && *sour != '0' && *dest != '\0')
	{
		dest++;
		sour++;
	}

	return (int)(*dest - *sour);
}

//strstr模拟实现
char* my_strstr(const char* haystack, const char* needle)
{
	assert(haystack != NULL && needle != NULL);

	int len_h = strlen(haystack);
	int len_n = strlen(needle);

	int i = 0;
	for (i = 0; i < len_h-len_n + 1; i++)
	{
		int flag = 0;
		int j = 0;
		for ( j = 0; j < len_n; j++)
		{
			if (haystack[i + j] != needle[j])
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			return haystack + i;
		}
	}
	
	return NULL;
}

//memcpy模拟实现
void* my_memcpy(void* str1, const void* str2, size_t n)
{
	assert(str1 != NULL && str2 != NULL);
	void* ret = str1;

	while (n--)
	{
		*(char*)str1 = *(char*)str2;
		(char*)str1 = (char*)str1 + 1;
		(char*)str2 = (char*)str2 + 1;
	}

	return ret;
}

//memmove模拟实现
void* my_memmove(void* str1, const void* str2, size_t n)
{
	assert(str1 != NULL && str2 != NULL);
	void* ret = str1;
	
	if (str1 < str2)
	{
		while (n--)
		{
			*(char*)str1 = *(char*)str2;
			(char*)str1 = (char*)str1 + 1;
			(char*)str2 = (char*)str2 + 1;
		}
	}
	else
	{
		while (n--)
		{
			*((char*)str1 + n) = *((char*)str2 + n);
		}
	}

	return ret;
}