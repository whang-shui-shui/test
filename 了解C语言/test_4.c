#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmp_int(void* e1, void* e2)
{
	return *(int*)e1 - *(int*)e2;
}

int cmp_char(void* e1, void* e2)
{
	return (int)(*(char*)e1 - *(char*)e2);
}

int test_4(void)
{
	int arr[] = { 5,1,3,6,7,2,7,8,2,4, };
	int len_int = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, len_int, 4, cmp_int);

	int i = 0;
	for ( i = 0; i < len_int; i++)
	{
		printf("%d ", arr[i]);
	}

	char charstr[] = "klkowieanfuwi";
	int len_char = strlen(charstr);
	qsort(charstr, len_char, 1, cmp_char);
	printf("\n%s\n", charstr);

	char str[][20] = { "afsd","546h8i", "hfu12","jiu9243","ifokj34" };
	int len_str = sizeof(str) / sizeof(str[0]);
	qsort(str, len_str, 20, strcmp);
	for ( i = 0; i < len_str; i++)
	{
		printf("%s ", str[i]);
	}

	return 0;
}