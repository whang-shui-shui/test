#define _CRT_SECURE_NO_WARNINGS
#include <stddef.h>
#include <stdio.h>

//自定义offsetof宏，计算结构体成员变量偏移量
#define MY_OFFSETOF(type, member) ((size_t)&(((type*)0)->member))

struct S
{
	char c;
	int a;
	char b;
};

int test_5(void)
{
	printf("%d", MY_OFFSETOF(struct S, a));
	return 0;
}