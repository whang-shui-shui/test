#pragma once

char* my_strncpy(char* dest, const char* sour, int num);
char* my_strncat(char* dest, const char* sour, int num);
int my_atoi(const char* str);
int my_strlen(const char* str);
char* my_strcpy(char* dest, const char* sour);
char* my_strcat(char* dest, const char* sour);
int my_strcmp(const char* dest, const char* sour);
char* my_strstr(const char* haystack, const char* needle);
void* my_memcpy(void* str1, const void* str2, size_t n);
void* my_memmove(void* str1, const void* str2, size_t n);
