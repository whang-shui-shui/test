#define _CRT_SECURE_NO_WARNINGS
#include "sort.h"
#include <stdio.h>

int test_2(void)
{
	int arr[] = { 9, 8, 7, 4, 6, 5, 3, 2, 1 };
	SelectSort(arr, 9);
	PrintArray(arr, 9);
}