#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int STDataType;

typedef struct Stack
{
	STDataType* data;
	int top;
	int capacity;
} Stack;

//初始化
void StackInit(Stack** pps);

//销毁
void StackDestroy(Stack** pps);

//入栈
void StackPush(Stack* ps, STDataType x);

//出栈
void StackPop(Stack* ps);

//获取栈顶元素
STDataType StackTop(Stack* ps);

//判空
_Bool StackEmpty(Stack* ps);

//获取大小
size_t StackSize(Stack* ps);