#define _CRT_SECURE_NO_WARNINGS
#include "sort.h"
#include "Stack.h"

//交换数据
void Swap(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//插入排序
void InsertSort(int* arr, int size)
{
	int end = 0;
	int i = 0;
	for (i = 0; i < size-1; i++)
	{
		end = i;
		int temp = arr[end + 1];

		while (end >= 0)
		{
			if (temp < arr[end])
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
			{
				break;
			}
		}
		arr[end + 1] = temp;
	}
}

//希尔排序
void ShellSort(int* arr, int size)
{
	int gap = size;
	while (gap > 1)
	{
		gap = gap / 3 + 1;	//gap每次除3，保证最后一次是1，则+1

		//gap大于1时是预排序，等于1时是直接插入排序
		int i = 0;
		for (i = 0; i < size - gap; i++)
		{
			int end = i;
			int temp = arr[end + gap];
			while (end >= 0)
			{
				if (temp < arr[end])
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			arr[end + gap] = temp;
		}
	}
}

//选择排序
void SelectSort(int* arr, int size)
{
	int begin = 0;
	int end = size - 1;

	while (begin < end)
	{
		int max = begin;
		int min = begin;
		int i = 0;
		for (i = begin+1; i <= end; i++)
		{
			if (arr[i] < arr[min])
			{
				min = i;
			}
			if (arr[i] > arr[max])
			{
				max = i;
			}
		}

		Swap(&arr[min], &arr[begin]);
		if (max == begin)
		{
			max = min;
		}
		Swap(&arr[max], &arr[end]);
		begin++;
		end--;
	}
}

//堆排序
void AdjustDown(int* arr, int n, int parent)
{

}

void HeapSort(int* arr, int size)
{

}


//冒泡排序
void BubbleSort(int* arr, int size)
{
	int i = 0;
	for (i = 0; i < size - 1; i++)
	{
		_Bool flag = 0;
		int j = 0;
		for (j = 0; j < size - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				Swap(&arr[j], &arr[j + 1]);
				flag = 1;
			}
		}

		if (flag == 0)
		{
			break;
		}
	}
}

//快速排序
//三值取中
int GetMidIndex(int* arr, int left, int right)
{
	int mid = (left + right) / 2;
	if (arr[left] < arr[right])
	{
		if (arr[mid] < arr[left])
		{
			return left;
		}
		else if (arr[mid] <arr[right])
		{
			return mid;
		}
		else
		{
			return right;
		}
	}
	else
	{
		if (arr[mid] < arr[right])
		{
			return right;
		}
		else if (arr[mid] < arr[left])
		{
			return mid;
		}
		else
		{
			return left;
		}
	}
}

//霍尔法
int PartSort1(int* arr, int left, int right)
{
	int key = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[key]);
	while (left < right)
	{
		//right找小
		while (left < right && arr[right] >= arr[key])	//只找小的，等于的要过滤掉，不然容易死循环
		{
			right--;
		}

		//left找大
		while (left < right && arr[left] <= arr[key])	//比它大的不用管
		{
			left++;
		}

		if (left < right)
			Swap(&arr[left], &arr[right]);
	}

	Swap(&arr[left], &arr[key]);	//相遇位置是r停下来的位置，小于key；相遇位置是l停下的位置，交换过后小于key
	return left;	//相遇位置返回
}

//挖坑法
int PartSort2(int* arr, int left, int right)
{
	int key = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[key]);
	
	key = arr[left];
	int hole = left;
	while (left < right)
	{
		while (left < right && arr[right] >= key)
		{
			right--;
		}
		arr[hole] = arr[right];
		hole = right;

		while (left < right && arr[left] <= key)
		{
			left++;
		}
		arr[hole] = arr[left];
		hole = left;
	}

	arr[hole] = key;
	return hole;
}

//前后指针
int PartSort3(int* arr, int left, int right)
{
	int key = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[key]);
	key = left;

	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (arr[cur] < arr[key] && ++prev != cur)
		{
			Swap(&arr[cur], &arr[prev]);
		}
		cur++;
	}

	Swap(&arr[key], &arr[prev]);
	return prev;
}

void QuickSort(int* arr, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}

	if (end - begin <= 8)
	{
		InsertSort(arr + begin, end - begin + 1);
	}
	else
	{
		int key = PartSort1(arr, begin, end);
		QuickSort(arr, begin, key - 1);
		QuickSort(arr, key + 1, end);
	}
}

//快速排序非递归
void QuickSortNonR(int* arr, int begin, int end)
{
	Stack *ps;
	StackInit(&ps);

	StackPush(ps, end);
	StackPush(ps, begin);

	while (!StackEmpty(ps))
	{
		int left = StackTop(ps);
		StackPop(ps);
		int right = StackTop(ps);
		StackPop(ps);

		if (left >= right)
		{
			continue;
		}

		int key = PartSort3(arr, left, right);

		StackPush(ps, right);
		StackPush(ps, key + 1);

		StackPush(ps, key - 1);
		StackPush(ps, left);
	}

	StackDestroy(&ps);
}

//归并排序


//打印整型数组
void PrintArray(int* arr, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}