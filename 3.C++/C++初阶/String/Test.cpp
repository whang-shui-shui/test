#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

void test_1(void)
{
	std::string s1;
	std::string s2("123456 ");
	std::string s3 = "45678";
	std::string s4(20, 'a');
	std::string s5(s2);
	std::string s6("123456789", 5);			// 截断取前5个
	std::string s7("123456789", 5, 3);		// 从第5个开始，取3个。超过字符串长度不报错，截到最后
	std::string s8(s2, 2);					// 从第5个开始，取3个。

	std::cout << s1 << std::endl;
	std::cout << s2 << std::endl;
	std::cout << s3 << std::endl;
	std::cout << s4 << std::endl;
	std::cout << s5 << std::endl;
	std::cout << s6 << std::endl;
	std::cout << s7 << std::endl;
	std::cout << s8 << std::endl;

	// 遍历
	// 小心越界，越界就报错
	for (size_t i = 0; i < s7.size(); ++i)
	{
		std::cout << s7[i] << std::endl;
	}

	// 安全一点
	for (const auto& e : s7)
	{
		std::cout << e << std::endl;
	}

	s2 += s3;
	s3 += "90890";
	std::cout << s2 << std::endl;
	std::cout << s3 << std::endl;
}

void test_2(void)
{
	std::string a = "123456789";
	const std::string b = "23545253245";

	std::cout << a.size() << std::endl;
	std::cout << a.length() << std::endl;
	std::cout << a.capacity() << std::endl;
	std::cout << a.empty() << std::endl;

	/*a.resize(20, 'd');
	std::cout << a << std::endl;
	a.clear();
	std::cout << a << std::endl;


	std::cout << a.capacity() << std::endl;
	a.reserve(90);
	std::cout << a.capacity() << std::endl;*/

	for (size_t i = 0; i < b.size(); i++)
	{
		std::cout << b[i] << " ";
	}

	std::cout << std::endl;

	std::cout << b.max_size() << std::endl;
}

// 大数相加
void test_3(void)
{
	std::string s1 = "1354984352168498513";
	std::string s2 = "8413549851365165";

	int end1 = s1.size() - 1, end2 = s2.size() - 1;
	std::string s3;
	s3.reserve(std::max(s1.size(), s2.size()) + 1);

	int carry = 0;
	while (end1 >= 0 || end2 >= 0)
	{
		int n1 = end1 >= 0 ? s1[end1] - '0' : 0;
		int n2 = end2 >= 0 ? s2[end2] - '0' : 0;

		int ret = n1 + n2 + carry;
		if (ret > 9)
		{
			ret -= 10;
			carry = 1;
		}
		else
		{
			carry = 0;
		}

		s3 += (char)(ret + '0');

		--end1;
		--end2;
	}

	if (carry != 0)
	{
		s3 += (char)(carry + '0');
	}

	std::reverse(s3.begin(), s3.end());
	std::cout << s3 << std::endl;
}

// 迭代器演示
void test_4(void)
{
	std::string s1 = "12335678";
	const std::string s2 = "12335678";
	std::string::iterator it1 = s1.begin();
	std::string::const_iterator it2 = s1.begin();
	std::string::const_iterator it4 = s2.begin();
}

using std::string;
class Solution {
public:
	bool isPalindrome(string s)
	{
		int begin = 0, end = s.size() - 1;
		while (begin < end)
		{
			while (begin < end && !(isArph(s[end]) || isNum(s[end])))
				--end;

			while (begin < end && !(isArph(s[begin]) || isNum(s[begin])))
				++begin;

			if (!isCmp(s[begin], s[end]))
				return false;

			++begin;
			--end;
		}
		return true;
	}

protected:
	bool isArph(const char& c)
	{
		return (c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A');
	}
	bool isNum(const char& c)
	{
		return (c <= '9' && c >= '0');
	}

	bool isCmp(const char& a, const char& b)
	{
		if (isNum(a) && isNum(b))
		{
			return a == b;
		}
		else
		{
			char c = 'a' - 'A';
			return a == b || a + c == b || a == b + c;
		}
	}
};

// 判断回文数
void test_5(void)
{
	Solution s;
	s.isPalindrome("A man, a plan, a canal: Panama");
}

// string类的查找演示
void test_6(void)
{
	std::string s1("12345");
	std::string s2("67890");
	std::string s3("");

	// find、substr、c_str演示
	/*std::cout << s1.find('5', 0) << std::endl;
	std::cout << s1.rfind('5', 0) << std::endl;
	std::cout << s1.substr(2, 9) << std::endl;
	const char* p = s1.c_str();
	char arr[6];
	strcpy(arr, s1.c_str());*/

	// 下标访问
	/*for (size_t i = 0; i < s1.size(); i++)
	{
		s1[i] += 1;
		std::cout << s1[i] << std::endl;
	}*/

	std::cout << s1.capacity() << std::endl;
	std::cout << sizeof(s1) << std::endl;
}

int main(void)
{
	//test_1();
	//test_2();
	//test_3();
	//test_4();
	//test_5();
	test_6();
	return 0;
}