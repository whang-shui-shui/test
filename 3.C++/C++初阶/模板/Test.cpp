#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

template<typename T>
void Swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}

int main(void)
{
	int a = 3;
	int b = 4;

	Swap(a, b);

	std::cout << a << " " << b << std::endl;

	return 0;
}