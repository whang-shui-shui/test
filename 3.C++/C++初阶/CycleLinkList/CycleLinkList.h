#pragma once
#include <iostream>

typedef int CListType;

typedef struct Node
{
	CListType data;
	struct Node* next;
} Node;

class CycleLinkList
{
public:
	CycleLinkList(void);						// 构造函数
	CycleLinkList(CycleLinkList& e);			// 拷贝构造

	bool Add(CListType e);						// 添加
	bool Remove(CListType e);					// 删除
	bool Alter(CListType dest, CListType e);	// 修改
	size_t Size(void);							// 长度
	bool Empty(void);							// 判空

	void Print(void);							// 打印内容

protected:
	Node* GetNode(CListType e);					// 获取节点
	Node* Find(CListType e);					// 查找节点
	~CycleLinkList(void);						// 析构函数

private:
	Node* _head;
	size_t _size;
};

