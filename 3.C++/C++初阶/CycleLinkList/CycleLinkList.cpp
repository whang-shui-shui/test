#include "CycleLinkList.h"

// 构造函数
CycleLinkList::CycleLinkList(void)
{

}

// 拷贝构造
CycleLinkList::CycleLinkList(CycleLinkList& e)
{

}

// 添加
bool CycleLinkList::Add(CListType e)
{

}

// 删除
bool CycleLinkList::Remove(CListType e)
{

}

// 修改
bool CycleLinkList::Alter(CListType dest, CListType e)
{

}

// 长度
size_t CycleLinkList::Size(void)
{

}

// 判空
bool CycleLinkList::Empty(void)
{

}

// 打印内容
void CycleLinkList::Print(void)
{

}

// 获取节点
Node* CycleLinkList::GetNode(CListType e)
{

}

// 查找节点
Node* CycleLinkList::Find(CListType e)
{

}

// 析构
CycleLinkList::~CycleLinkList(void)
{

}