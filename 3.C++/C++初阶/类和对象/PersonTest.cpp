#define _CRT_SECURE_NO_WARNINGS
#include "Person.h"

Person GetPerson(void)
{
	Person a("zhang");
	return a;
}

Person GetPersonNoName(void)
{
	return Person("zhang");
}

Person GetPersonNumber(void)
{
	return "zhang";
}

int main(void)
//int TestPerson(void)
{
	Person* p = new Person("zhang");
	Person* m = (Person*)malloc(sizeof(Person));
	free(m);
	delete p;
	return 0;
}