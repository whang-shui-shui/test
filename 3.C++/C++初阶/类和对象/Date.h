//#include <iostream>
//
//class Date
//{
//	// 友元
//	friend std::ostream& operator<<(std::ostream& out, const Date& d);	// 流插入运算符
//	friend std::istream& operator>>(std::istream& in, Date& d);			// 流提取运算符
//
//public:
//	// 默认成员函数
//	Date(int year=1970, int month=1, int day=1);						// 默认构造函数
//	~Date(void);														// 析构函数
//	Date(const Date& d);												// 拷贝构造
//	Date& operator=(const Date& d);										// 赋值重载
//	Date* operator&(void);												// 取地址重载
//	const Date* operator&(void) const;									// 常取地址重载
//	
//	// 运算符重载
//	bool operator==(const Date& d) const;								// ==
//	bool operator!=(const Date& d) const;								// !=
//	bool operator>(const Date& d) const;								// >
//	bool operator>=(const Date& d) const;								// >=
//	bool operator<(const Date& d) const;								// <
//	bool operator<=(const Date& d) const;								// <=
//	
//	Date& operator+=(int day);											// +=
//	Date operator+(int day) const;										// +
//	Date& operator-=(int day);											// -=
//	Date operator-(int day) const;										// -
//	
//	Date& operator++(void);												// 前置++
//	Date operator++(int);												// 后置++
//	Date& operator--(void);												// 前置--
//	Date operator--(int);												// 后置--
//	
//	int operator-(const Date& d) const;									// 日期-日期
//	
//	// 功能实现
//	void Print(void) const;												// 打印日期
//	
//protected:
//	int GetDayofMonth(int year, int month) const;						// 获取月分的天数
//	
//private:
//	int _year;		// 年
//	int _month;		// 月
//	int _day;		// 日
//};