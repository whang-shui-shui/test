//#include "Date.h"
//
//// 默认成员函数
//// 默认构造函数
//Date::Date(int year, int month, int day)
//	: _year(year)
//	, _month(month)
//	, _day(day)
//{
//	// 判断日期不合法
//	if (year < 0
//		&& (month > 12 || month < 1)
//		&& (day < 1 || day > GetDayofMonth(year, month)))
//	{
//		_year = 1970;
//		_month = 1;
//		_day = 1;
//		perror("Set Date Failure!\n");
//	}
//}
//
//// 析构函数
//Date::~Date(void)
//{
//	_year = 1970;
//	_month = 1;
//	_day = 1;
//}
//
//// 拷贝构造
//Date::Date(const Date& d)
//{
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//}
//
//// 赋值重载
//Date& Date::operator=(const Date& d)
//{
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//
//	return *this;
//}
//
//// 取地址重载
//Date* Date::operator&(void)
//{
//	return this;
//}
//
//// 常取地址重载
//const Date* Date::operator&(void) const
//{
//	return this;
//}
//
//
//
//// 运算符重载
//// 等于 ==
//bool Date::operator==(const Date& d) const
//{
//	return _year == d._year
//		&& _month == d._month
//		&& _day == d._day;
//}
//
//// 不等于 !=
//bool Date::operator!=(const Date& d) const
//{
//	return !(*this == d);
//}
//
//// 大于 >
//bool Date::operator>(const Date& d) const
//{
//	if (_year > d._year)
//	{
//		return true;
//	}
//	else if (_year == d._year && _month > d._month)
//	{
//		return true;
//	}
//	else if (_year == d._year && _month == d._month && _day > d._day)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}
//
//// 大于等于 >=
//bool Date::operator>=(const Date& d) const
//{
//	return (*this > d || *this == d);
//}
//
//// 小于 <
//bool Date::operator<(const Date& d) const
//{
//	return !(*this >= d);
//}
//
//// 小于等于 <=
//bool Date::operator<=(const Date& d) const
//{
//	return !(*this > d);
//}
//
//// 日期+=天数
//Date& Date::operator+=(int day)
//{
//	if (day < 0)
//	{
//		return ((*this) -= (-day));
//	}
//
//	_day += day;		// 加上日期
//	int temp = 0;		// 用来保存该月天数，进位时减天数不用再次调用函数，提高效率
//
//	// 判断日期是否溢出，并进位
//	while (_day > (temp = GetDayofMonth(_year, _month)))
//	{
//		_day -= temp;
//		_month++;
//		if (_month > 12)
//		{
//			_month = 1;
//			_year++;
//		}
//	}
//
//	return *this;
//}
//
//// 日期+天数
//Date Date::operator+(int day) const
//{
//	Date e = *this;
//	e += day;
//	return e;
//}
//
//// 日期-=天数
//Date& Date::operator-=(int day)
//{
//	if (day < 0)
//	{
//		return ((*this) += (-day));
//	}
//
//	_day -= day;
//	while (_day < 1)
//	{
//		_month--;
//		if (_month < 1)
//		{
//			_year--;
//			_month = 12;
//		}
//
//		_day += GetDayofMonth(_year, _month);
//	}
//
//	return *this;
//}
//
//// 日期-天数
//Date Date::operator-(int day) const
//{
//	Date e = *this;
//	e -= day;
//	return e;
//}
//
//// 前置++
//Date& Date::operator++(void)
//{
//	return (*this += 1);
//}
//
//// 后置++
//Date Date::operator++(int)
//{
//	Date e = *this;
//	*this += 1;
//	return e;
//}
//
//// 前置--
//Date& Date::operator--(void)
//{
//	return (*this -= 1);
//}
//
//// 后置--
//Date Date::operator--(int)
//{
//	Date e = *this;
//	*this -= 1;
//	return e;
//}
//
//// 日期-日期
//int Date::operator-(const Date& d) const
//{
//	Date max = *this;
//	Date min = d;
//	int flag = 1;
//
//	if (max < min)
//	{
//		max = d;
//		min = *this;
//		flag = -1;
//	}
//
//	int count = 0;
//	while (min < max)
//	{
//		++min;
//		++count;
//	}
//
//	return flag * count;
//}
//
//
//
//// 打印日期
//void Date::Print(void) const
//{
//	std::cout << _year << "/" << _month << "/" << _day << std::endl;
//}
//
//
//// 获取月分的天数
//int Date::GetDayofMonth(int year, int month) const
//{
//	int arr[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
//	if (month == 2
//		&& (year % 4 == 0 && year % 100 != 0 || year % 400 == 0))
//	{
//		arr[2]++;
//	}
//
//	return arr[month];
//}
//
//// 流插入运算符
//inline std::ostream& operator<<(std::ostream& out, const Date& d)
//{
//	out << d._year << "/" << d._month << "/" << d._day;
//	return out;
//}
//
//// 流提取运算符
//inline std::istream& operator>>(std::istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//	return in;
//}