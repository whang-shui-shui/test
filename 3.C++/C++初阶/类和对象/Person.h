#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <string.h>
#include <iostream>

class Person
{
public:
	// 静态函数
	static int GetN(void)
	{
		return s_N;
	}

	static void SetN(int n)
	{
		s_N = n;
	}

	//默认构造函数
	Person(void)
		: _name(nullptr)
		, _id(0)
		, _age(0)
	{
		std::cout << "Person 默认构造函数" << std::endl;
		_name = (char*)calloc(10, sizeof(char));
		if (_name == nullptr)
		{
			perror("malloc fail\n");
			exit(EXIT_FAILURE);
		}
	}

	// 全参构造函数
	Person(const char* name, int id = 0, int age = 0)
		: _name(nullptr)
		, _id(id)
		, _age(age)
	{
		std::cout << "Person 全参构造函数" << std::endl;
		_name = (char*)calloc(10, sizeof(char));
		if (_name == nullptr)
		{
			perror("malloc fail\n");
			exit(EXIT_FAILURE);
		}
	}

	// 拷贝构造
	Person(const Person& p)
		: _name(nullptr)
		, _id(p._id)
		, _age(p._age)
	{
		std::cout << "Person 拷贝构造函数" << std::endl;
		_name = (char*)calloc(10, sizeof(char));
		if (_name == nullptr)
		{
			perror("malloc fail\n");
			exit(EXIT_FAILURE);
		}
		strcpy(_name, p._name);
	}

	// 赋值重载
	Person& operator=(const Person& p)
	{
		std::cout << "Person 赋值重载函数" << std::endl;
		_age = p._age;
		_id = p._id;
		strcpy(_name, p._name);
	}

	// ==操作符重载
	bool operator==(const Person& e) const
	{
		return (_age == e._age && _id == e._id);
	}

	// > 操作符重载
	bool operator>(const Person& e) const
	{
		if (_age > e._age)
		{
			return true;
		}
		else if (_age == e._age && _id > e._id)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// >= 操作符重载
	bool operator>=(const Person& e) const
	{
		return *this > e || *this == e;
	}

	// 析构函数
	~Person(void)
	{
		std::cout << "Person 析构函数" << std::endl;
		free(_name);
		_name = NULL;
		_age = 0;
		_id = 0;
	}

private:
	char* _name;
	int _age;
	int _id;
	static int s_N;
};

int Person::s_N = 100;