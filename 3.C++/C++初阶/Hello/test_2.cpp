#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

extern int add(int a = 0, int b = 1, int c = 9);


//半缺省  只能从右往左连续
int sub(int a, int b = 0, int c = 0)
{
	return a - b - c;
}

int test_2(void)
{
	int a = 0;
	std::cin >> a;

	int sum = add(a, 2);
	int div = sub(a, 2);

	std::cout << sum << " " << div << std::endl;
	
	return 0;
}