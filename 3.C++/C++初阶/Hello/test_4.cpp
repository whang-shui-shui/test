#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

extern int add(int a = 1, int b = 2, int c = 3);
extern int _Add();

namespace test4
{
	// ��������
	int div(int a)
	{
		std::cout << "int div(int a)" << std::endl;
		return a;
	}

	int div(int a, int b = 2)
	{
		std::cout << "int div(int a, int b)" << std::endl;
		return a / b;
	}

	double div(double a = 2.9, double b = 1.2)
	{
		std::cout << "double div(double a, double b)" << std::endl;
		return a / b;
	}

	char div(char a = '2', char b = '3')
	{
		std::cout << "char div(char a, char b)" << std::endl;
		return a / b;
	}

}

int a = 1;
namespace Name1
{
	int a = 100;
}
namespace Name2
{
	int a = 249;
	void Print(void)
	{
		printf("%d\n", a);			//249
		printf("%d\n", Name1::a);	//100
		printf("%d\n", ::a);		//1
	}
}

int test_4()
{
	/*test4::div(1, 2);
	test4::div(1.1, 2.1);
	test4::div('2', '2');*/

	std::cout << add() << std::endl;
	std::cout << _Add() << std::endl;

	//Name2::Print();
	return 0;
}
