#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

namespace test5
{
	int add(int a, int b)
	{
		return a + b;
	}

	int add(int& a, int& b)
	{
		return a + b;
	}
}


int test_5()
{
	
	const int a = 0;
	double b = 9.0;
	const int& ra = a;
	const int& rra = (int)b;
	std::cout << rra << std::endl;
	return 0;
}