#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "test_6.h"


//内联函数的调用
void test1(void)
{
	int a = 5;
	int b = 2;
	int temp = 20;

	test6::Swap(a, b);

	std::cout << a << " " << b << std::endl;
	std::cout << temp << std::endl;
}

//auto
void test2(void)
{
	int a = 20;
	auto b = a;

	int arr[] = { 1,2,3,4,5,6 };
	for (auto e : arr)
	{
		std::cout << e << std::endl;
	}
}

int main(void)
{
	test2();
	return 0;
}

namespace test6
{
	inline void Swap(int& a, int& b)
	{
		int temp = a;
		a = b;
		b = temp;
		std::cout << temp << std::endl;
	}
}