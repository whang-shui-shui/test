#define _CRT_SECURE_NO_WARNINGS

//两数求和
int Add(int a, int b)
{
	return a + b;
}

//两数求差
int Sub(int a, int b)
{
	return a - b;
}

//按顺序打印数字的每个数字（迭代，从后往前）
void PrintNumberForOder(unsigned int num)
{
	while (0 != num)
	{
		printf("%u ", num % 10);
		num /= 10;
	}
}

//按顺序打印数字的每个数字（递归，从前往后）
void PrintNumberByMyself(unsigned int num)
{
	if (num > 9)
	{
		PrintNumberByMyself(num / 10);
	}
	printf("%d ", num % 10);
}

//求字符串长度(迭代)
//int StringLength(char* str)
//{
//	int count = 0;
//
//	while ('\0' != *str)
//	{
//		count++;
//		str++;
//	}
//
//	return count;
//}

//求字符串长度（递归）
/*要求abc，先求ab+c，
要求ab，先求a+b*/
int StringLengthByMyself(char* str)
{
	if ('\0' != *str)
	{
		return StringLengthByMyself(str + 1) +1;
	}
	else
	{
		return 0;
	}
	
}

//阶乘递归
int Fac(int n)
{
	if (n > 1)
	{
		return n * Fac(n - 1);
	}
	else
	{
		return 1;
	}
}

//第n个斐波那契数（递归）
int FibByMyself(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	else
	{
		return Fib(n - 1) + Fib(n - 2);
	}
}

//第n个斐波那契数，迭代
int Fib(int n)
{
	int a = 1;
	int b = 1;
	int c = 0;
	while (n > 2)
	{
		c = a + b;
		a = b;
		b = c;
		n--;
	}

	return c;
}

//汉诺塔
void H(int n)
{
	if (0 == n)
	{

	}
	else
	{
		H(n - 1);
	}
}