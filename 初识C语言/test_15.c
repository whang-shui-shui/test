#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>

//数组，初始化、打印、反转函数
void init(int arr[], int length)
{
	assert(length > 0);
	int i = 0;
	for (i = 0; i < length; i++)
	{
		arr[i] = 0;
	}
}

void print(int arr[], int length)
{
	assert(length > 0);
	int i = 0;
	for (i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void reverse(int arr[], int length)
{
	assert(length > 0);
	int left = 0;
	int right = length - 1;
	while (left < right)
	{
		int temp = arr[left];
		arr[left] = arr[right];
		arr[right] = temp;
		left++;
		right--;
	}
}

int test_15(void)
{
	int arr[10];
	init(arr, 10);
	print(arr, 10);
	arr[2] = 10;
	arr[6] = 9;
	reverse(arr, 10);
	print(arr, 10);
	return 0;
}