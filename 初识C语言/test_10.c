#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include "tool_1.h"

//包含头文件tool的演示
int test_10(void)
{
	printf("%d\n", Add(3, 8));
	printf("%d\n", Sub(3, 8));

	PrintNumberForOder(1234);
	printf("\n");
	PrintNumberByMyself(1234);
	printf("\n");


	//printf("%d\n", StringLength("12345000"));
	printf("%d\n", StringLengthByMyself("12345000"));

	printf("%d\n", Fac(5));

	printf("%d\n", FibByMyself(8));
	printf("%d\n", Fib(8));
	return 0;
}