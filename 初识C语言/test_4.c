#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>

//
int test_4(void)
{
	int num = srand(time(0)) % 100;
	if (num < 0)
	{
		num = -num;
	}
	int n = 0;

	do
	{	
		scanf("%d", &n);
		if (n > num)
		{
			printf("大了\n");
		}
		else if (n < num)
		{
			printf("小了\n");
		}
		
	} while (n != num);

	printf("你真棒\n");

	return 0;
}