#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int binery_search(int* arr, int key, int size);

//折半查找法（二分查找法）
int test_3(void)
{
	int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int key = 6;
	int sz = sizeof(arr) / sizeof(arr[0]);

	int index = binery_search(arr, key, sz);

	if (-1 != index)
	{
		printf("%d的下标是%d\n", key, index);
	}
	else
	{
		printf("没找到\n");
	}
	
	return 0;
}

//二分查找法
int binery_search(int* arr, int key, int size)
{
	int left = 0;
	int right = size - 1;
	int mid = 0;

	while (left <= right)
	{
		mid = (left + right) / 2;

		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}