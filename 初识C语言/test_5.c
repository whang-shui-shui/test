#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>

//int main(void)
//{
//	char str[] = "123458090";
//	char str2[20];
//	//等待1秒
//	Sleep(1000);
//
//	//字符串比较
//	strcmp(str, "14242");
//	//字符串拷贝
//	strcpy(str2, str);
//	//字符串str第3个字符开始，替换为x，替换5个
//	memset(str2+3, 'x', 5);
//
//	//清屏
//	system("cls");
//	//60秒后关机
//	system("shutdown -s -t 60");
//	//停止计时关机
//	system("shutdown -a");
//	return 0;
//}

void menu();

int test_5(void)
{
	srand((unsigned int)time(NULL));
	int num = rand() % 1000;

	int n = 0;

	menu();
	system("shutdown -s -t 60");

	while (1)
	{
		printf("请您输入 0 到 1000 之内的数字:");
		scanf("%d", &n);

		if (num > n)
		{
			printf("猜小了，继续加油\n");
		}
		else if (num < n)
		{
			printf("猜大了，继续加油\n");
		}
		else
		{
			system("shutdown -a");
			printf("恭喜你，猜对啦\n");
			break;
		}
	}

	return 0;
}

void menu() 
{
	printf("************************************\n");
	printf("***** 你好，欢迎玩猜数字小游戏 *****\n");
	printf("**** 您的电脑将在60秒后自动关机 ****\n");
	printf("**** 猜中正确的数字就能解除关机 ****\n");
	printf("********** 祝您游戏愉快 ************\n");
	printf("************************************\n");
}