﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int* getNum(int n);					//获取该数的约数数组
int* getSame(int arr[], int brr[]);	//获取两个数组相同部分
int getMax(int arr[]);				//获取数组最大值

//计算两个数的最大公约数
int test_0()
{
	//输入a、b两个数
	int a, b;
	scanf("%d %d", &a, &b);
	printf("%d %d\n", a, b);

	//获取a和b的约数数组
	int* arr = getNum(a);
	int* brr = getNum(b);

	//获取两个约数数组的共同部分，得到数组all
	int* all = getSame(arr, brr);

	//获取all数组的最大值，既是最大公约数，并打印
	int max = getMax(all);
	printf("%d", max); 

	//释放内存
	free(arr);
	free(brr);
	free(all);
	return 0;
}

//获取该数的约数数组，该数组使用0下标位置存储约数的个数
int* getNum(int n)
{
	int* arr = (int*)malloc(20 * sizeof(int));
	arr[0] = 0;

	for (int i = 1; i < sqrt(n); i++)
	{
		if (n % i == 0)
		{
			arr[arr[0] + 1] = i;
			arr[arr[0] + 2] = n / i;
			arr[0] += 2;
		}
	}
	return arr;
}

//获取数组最大值，0下标存储约束的个数
int getMax(int arr[])
{
	int max = arr[1];
	for (int i = 2; i <= arr[0]; i++)
	{
		if (max < arr[i])
		{
			max = arr[i];
		}
	}
	return max;
}

//获取两个数组相同部分，并返回数组
int* getSame(int arr[], int brr[])
{
	int* all = (int*)malloc(10 * sizeof(int));
	all[0] = 0;
	for (int i = 1; i <= arr[0]; i++)
	{
		for (int j = 1; j <= brr[0]; j++)
		{
			if (arr[i] == brr[j])
			{
				all[0] += 1;
				all[all[0]] = arr[i];
			}
		}
	}
	return all;
}