#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "tool_sort.h"		//打印一维数组调用的头文件

int test_13(void)
{
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[10] = { 11,22,33,44,55,66,77,88,99,1010, };
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		int temp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = temp;
	}
	
	print_array_1(arr1, 10);	//打印一维数组
	print_array_1(arr2, 10);
	return 0;
}