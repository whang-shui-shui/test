#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
/*
* 主函数有三个参数
int main(int argc, char* argc[], char* envp[])
{
	return 0;
}

返回值类型和返回语句可以省略，不报错，but very 挫，千万不能省略
test()			无返回值类型也可以接收到返回值
{
	表达式1；
	表达式2；
	……
	表达式n；	无返回值语句，默认返回最后一条语句执行结果
}

调用无参函数也可以传递参数，不报错，but very 挫
调用参数为void的无参数函数会警告，但是不报错
test(100, 11);
int test(void);
*/

//void PrintArr_2(int** arr, int i, int j);
//
//int main(void)
//{
//	int arr[3][4] = { 1,2,3 ,4,5,6,7};
//	PrintArr_2(arr, 3, 4);
//	return 0;
//}
//
//void PrintArr_2(int** arr, int i, int j)
//{
//	int a = 0;
//	int b = 0;
//	for (a = 0; a < i; a++)
//	{
//		for (b = 0; b < j; b++)
//		{
//			printf("%d ", arr[a][b]);
//		}
//		printf("\n");
//	}
//}