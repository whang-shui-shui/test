#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//求字符串长度
int StringLength(char* string)
{
	if (*string == '\0')
	{
		return 0;
	}
	else
	{
		return 1 + StringLength(string + 1);
	}
}

//反转字符串
void reverse_string(char* string)
{
	int length = StringLength(string);
	if (length > 1)
	{
		char temp = string[0];
		string[0] = string[length - 1];
		string[length - 1] = '\0';
		reverse_string(string + 1);
		string[length - 1] = temp;
	}
}

int test_12(void)
{
	char arr[] = "12345678";
	reverse_string(arr);
	printf("%s\n", arr);
	return 0;
}