#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

_Bool is_leap_year(int y);

int test_6(void)
{
	int i = 0;
	for (i = 1000; i <= 2000; i++)
	{
		if (is_leap_year(i))
		{
			printf("%d ", i);
		}
	}
	return 0;
}

//判断是否是闰年
_Bool is_leap_year(int y)
{
	//十年一润，百年不润，四百一润
	if ((y % 4 == 0) && (y % 100 != 0) || (y % 100 == 0))
	{
		return 1;
	}
	return 0;
}