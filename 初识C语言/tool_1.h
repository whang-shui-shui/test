#pragma once

int Add(int a, int b);
int Sub(int a, int b);
void PrintNumberForOder(unsigned int num);
void PrintNumberByMyself(unsigned int num);
int StringLength(char* str);
int StringLengthByMyself(char* str);
int Fac(int n);
int Fib(int n);