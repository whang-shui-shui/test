#define _CRT_SECURE_NO_WARNINGS

//冒泡排序法
void bubble_sort(int arr[], int length)
{
	int i = 0;
	for (i = 0; i < length-1; i++)
	{
		int j = 0;
		for (j = 0; j < length-1-i; j++)//j<length-1-i,10，i为5判断9-5次
		{
			if (arr[j] > arr[j + 1])
			{
				int temp = arr[j+1];
				arr[j+1] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

//打印一维数组
void print_array_1(int arr[], int length)
{
	int i = 0;
	for (i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

//打印二维数组
void print_array_2(int *arr[], int a, int b)
{
	int i = 0;
	for (i = 0; i < a; i++)
	{
		int j = 0;
		for (j = 0; j < b; j++)
		{
			printf("%d ", arr[i][j]);
		}
	}
	printf("\n");
}