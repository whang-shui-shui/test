#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//最大公约数

//暴力求解法
int BF(int a, int b)
{
	int i = 0;
	for (i = a; i > 0; i--)
	{
		if (a % i == 0 && b % i == 0)
		{
			return i;
		}
	}
	return 0;
}

//辗转相除法
int Div(int a, int b)
{
	int c = 0;

	while ((c = a % b) != 0)
	{
		a = b;
		b = c;
	}

	return b;
}

int test_16(void)
{
	printf("%d\n", BF(20, 30));
	printf("%d", Div(20, 30));
	return 0;
}