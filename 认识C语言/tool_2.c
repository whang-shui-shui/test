#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>

char* my_strcpy(char* destination, const char* source)
{
	assert(destination != NULL && source != NULL);

	char* ret = destination;

	while (*destination = *source)
	{
		;
	}

	return ret;
}

char* my_strcat(char* destination, const char* source)
{
	assert(destination != NULL && source != NULL);

	char* ret = destination;

	while (*destination++ != '\0')
	{
		;
	}

	while (*destination++ = *source++)
	{
		;
	}

	return ret;
}

int my_strcmp(const char* str1, const char* str2)
{
	return 0;
}

char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 != NULL && str2 != NULL);

	const char* s1 = str1;
	const char* s2 = str2;
	const char* p = str1;
	while (*p != '\0')
	{
		s1 = p;
		s2 = str2;
		while (s1!='\0' && s2!='\0' && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
		{
			return p;
		}
		p++;
	}

	return NULL;
}

void* my_memcpy(void* destination, const void* source, int size_t)
{
	assert(destination != NULL && source != NULL);

	void* ret = destination;
	while (size_t--)
	{
		*(char*)destination = *(char*)source;
		destination = ((char*)destination) + 1;
		source = ((char*)source) + 1;
	}

	return ret;
}

void* my_memmove(void* destination, const void* source, int size_t)
{
	assert(destination != NULL && source != NULL);

	void* ret = destination;
	
	if (destination < source)
	{
		//ǰ����>��
		while (size_t--)
		{
			*(char*)destination = *(char*)source;
			destination = ((char*)destination) + 1;
			source = ((char*)source) + 1;
		}
	}
	else
	{
		//�󡪡�>ǰ
		while (size_t--)
		{
			*((char*)destination + size_t) = *((char*)source + size_t);
		}
	}
	return ret;
}