#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int test_4(void)
{
	char arr1[] = "12345";
	char* arr2 = "12345";
	printf("%p\n", arr1);//006FFAE0，该字符串存储在栈中开辟的数组里,一共开辟了6B空间
	printf("%p\n", arr2);//004D8B9C，该字符串在字符串常量区中，一共开辟了6B+4B空间（字符串(字符串常量区)+指针arr2(栈)）
	return 0;
}