#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int test_10(void)
{
    int n = 0;
    scanf("%d", &n);
    int* arr = (int*)malloc(n * sizeof(int));
    int j = 0;
    for (j = 0; j < n; j++)
    {
        scanf("%d", &arr[j]);
    }

    int flag1 = 0;
    int flag2 = 0;
    int i = 0;
    for (i = 1; i < n; i++)
    {
        if (arr[i - 1] < arr[i])
        {
            flag1 = 1;
        }
        else if (arr[i - 1] > arr[i])
        {
            flag2 = 1;
        }
        else
        {
            ;
        }
    }

    if (flag1 + flag2 == 1)
    {
        printf("sorted\n");
    }
    else
    {
        printf("unsorted");
    }

    free(arr);
    return 0;
}