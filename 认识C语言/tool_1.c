#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//交换两个变量
void Swap(const void* e1, const void* e2, const int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		char temp = *((char*)e1 + i);
		*((char*)e1 + i) = *((char*)e2 + i);
		*((char*)e2 + i) = temp;
	}
}

void _bubble_sort(const void* base, const int length, const int size, int (*cmp)(const void* e1, const void* e2))
{
	int i = 0;
	for (i = 0; i < length - 1; i++)
	{
		int flag = 1;
		int j = 0;
		for (j = 0; j < length - 1 - i; j++)
		{
			if (cmp((char*)base + j * (size), (char*)base + (j + 1) * (size)) > 0)
			{
				Swap((char*)base + j * (size), (char*)base + (j + 1) * (size), size);
				flag = 0;
			}
		}
		if (flag == 1)
		{
			break;
		}
	}
}

void _print_arr_int(const int arr[], const int length)
{
	int i = 0;
	for (i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
