#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void revove(char* const arr, int start, int end) 
{
	while (start < end)
	{
		char temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;
		start++;
		end--;
	}
}

void zhuan(char* const arr, int k)
{
	int length = strlen(arr);
	revove(arr, 0, k - 1);
	revove(arr, k, length - 1);
	revove(arr, 0, length - 1);
}

int jadge(char* arr1, char* arr2, int k)
{
	zhuan(arr1, k);
	return strcmp(arr1, arr2)==0? 1: 0;
}

int test_9(void)
{
	char str[] = "ABCDEFG";
	char str2[] = "CDEFGAB";
	int a = jadge(str, str2, 1);
	printf("%d %s %s", a, str, str2);
	return 0;
}