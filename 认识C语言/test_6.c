#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//阶乘
int Factorial(int n)
{
	if (n == 0 || n == 1)
	{
		return 1;
	}
	int ret = 1;
	int i = 0;
	for (i = 2; i <= n; i++)
	{
		ret = i * ret;
	}
	return ret;
}

//组合数
int C(int m, int n)
{
	int a = Factorial(n);
	int b = Factorial(m);
	int c = Factorial(n - m);
	return a / (b * c);
}

//杨辉三角
int test_6(void)
{
	int n = 0;
	scanf("%d", &n);
	
	int i = 1;
	for (i = 0; i < n; i++)
	{
		int j = 1;
		for (j = 0; j <= i; j++)
		{
			printf("%d ", C(j, i));
		}
		printf("\n");
	}
	return 0;
}

/*
	1			组合数C(0, i)到C(i, i)
	1 1			性质：C(m, n) = C(m-1, n-1) + C(m-1, n);
	1 2 1		公式：C(m, n) = n! / (m! * (n-m)!)
	1 3 3 1
	1 4 6 4 1
*/