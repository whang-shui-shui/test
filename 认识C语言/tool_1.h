#pragma once

//冒泡排序法
void _bubble_sort(const void* base, const int length, const int size, int (*cmp)(const void* e1, const void* e2));

//打印整型一维数组
void _print_arr_int(const int arr[], const int length);

//交换两个变量的内容
void Swap(const void* e1, const void* e2, const int size);
