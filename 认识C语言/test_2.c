#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//(*(void(*)())0)();
//void(*sinal(int, void(*)()))(int);
//void(*)(int) sinal2(int, void(*)());函数返回值类型不是放在前面，而是以函数声明作为变量名

int Add(int x, int y)
{
	return x + y;
}

int Sub(int x, int y)
{
	return x - y;
}

int Mul(int x, int y)
{
	return x * y;
}

int Div(int x, int y)
{
	return x / y;
}

//函数指针做参数
void compute(int (*com)(int, int))
{
	int x = 0;
	int y = 0;
	printf("Plese input your mnuber of two;>");
	scanf("%d %d", &x, &y);
	printf("%d\n", (*com)(x, y));
	//printf("%d\n", com(x, y));效果与上面一样
}

int test_2(void)
{
	int num = 1;
	int (*arr[5])(int, int) = { 0, Add, Sub, Mul, Div };//函数数组，叫转移表
	int (*(*arr_p)[5])(int, int) = arr;
	while (num)
	{
		printf("Plese select your maniplate:>");
		scanf("%d", &num);

		/*switch (num)
		{
		case 0:
			printf("exst\n");
			break;
		case 1:
			compute(Add);
			break;
		case 2:
			compute(Sub);
			break;
		case 3:
			compute(Mul);
			break;
		case 4:
			compute(Div);
			break;
		default:
			printf("错误\n");
			break;
		}*/

		
		if (num < 5 && num>0)
		{
			int x = 0, y = 0;
			printf("Plese input your mnuber of two;>");
			scanf("%d %d", &x, &y);
			printf("%d\n", arr[num](x, y));
		}
		else if (0 == num)
		{
			printf("exst\n");
		}
		else
		{
			printf("error\n");
		}
	}
	
	return 0;
}