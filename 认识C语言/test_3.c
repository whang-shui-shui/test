#define _CRT_SECURE_NO_WARNINGS
#include "tool_1.h"
int cmp(const void* e1, const void* e2);


int cmp(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}

int test_3(void)
{
	int arr[] = { 3,2,1,4,5,6,9,8,7 };
	int sz = (int)sizeof(arr) / sizeof(arr[0]);
	int (*cmp_p)(const void*, const void*) = cmp;
	_bubble_sort(arr, sz, (int)sizeof(int), cmp_p);
	_print_arr_int(arr, sz);
	return 0;
}