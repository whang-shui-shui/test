#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void point(int arr[])
{
	
}

int test_1(void)
{
	//const char* p = "1234";//字符指针
	//char* p2 = 'a';//p2是一个指针，指针指向char类型
	////数组名就是指针变量，但是有2个例外（sizeof单独放一个数组名，&数组名表示的是整个地址，打印出的第一个元素地址表示整个元素地址）
	////变量名先与*结合就是指针，先与[]结合就是数组，[]优先级大于*
	//char* arr1[3] = { 0 };//指针数组，arr1是一个数组，数组有3个元素，每个元素是指针，指针指向char类型
	//char(*arr2)[3] = 0;//数组指针，arr2是一个指针，指向数组，数组有3个元素，每个元素是char类型

	//int arr1[5] = { 1,2,3,4,5 };
	//int arr2[5] = { 6,7,8,9,10 };
	//int arr3[5] = { 0,9,8,7,6 };

	//int(*ap)[5] = &arr1;//数组指针
	//int (*ap_arr[3])[5] = { &arr1,&arr2,&arr3 };//数组指针数组
	//int(*(*aparr_ap)[5])[5] = &ap_arr;//数组指针数组指针
	//
	////int* arr = &arr1;有警告，但是不报错
	////int** arr = &arr1;有警告，但是不报错

	//int i = 0;
	//for (i = 0; i < 3; i++)
	//{
	//	for (int j= 0; j < 5; j++)
	//	{
	//		printf("%d ", (*ap_arr[i])[j]);
	//	}
	//	printf("\n");
	//}

	int arr1[3];
	int arr2[3][5];

	int* p1;
	int** p2;

	int* p_arr[3];
	int(*arr_p)[3];

	//int fun(int a,int b);
	int (*fun_p)(int, int);
	//int* fun(int a, int b);
	return 0;
}