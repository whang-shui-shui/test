#define _CRT_SECURE_NO_WARNINGS
#include <string.h>
#include <assert.h>
//反转字符数组
void reverse(char* left, char* right)
{
    while (left < right)
    {
        char temp = *left;
        *left = *right;
        *right = temp;
        left++;
        right--;
    }
}

//旋转字符串
void rotate(char* const str, int k)
{
    assert(str != NULL && k > 0);
    int len = strlen(str);
    k %= len;

    reverse(str, str + k - 1);
    reverse(str + k, str + len - 1);
    reverse(str, str + len - 1);
}

int test_11(void)
{
    int arr[10] = { 1,2,0,8,4,7,5,6,3,9 };
    char str[] = "123456";
    rotate(str, 3);
    printf("%s", str);
	return 0;
}