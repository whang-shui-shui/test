#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int LTDataType;
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
	struct ListNode* prev;
} ListNode;

//初始化
extern ListNode* ListInit(void);

//动态申请结点 Buy ListNode
extern ListNode* BuyListNode(LTDataType x);

//判空
extern _Bool ListEmpty(ListNode* plist);

//获取链表的长度
extern size_t ListSize(ListNode* plist);

//打印 Print
extern void ListPrint(ListNode* plist);

//尾插
extern void ListPushBack(ListNode* plist, LTDataType x);

//头插
extern void ListPushFront(ListNode* plist, LTDataType x);

//尾删
extern void ListPopBack(ListNode* plist);

//头删
extern void ListPopFront(ListNode* plist);

//销毁
extern void ListDetroy(ListNode* plist);

//查找
extern ListNode* ListFind(ListNode* plist, LTDataType x);

//在pos之前插入
extern void ListInsert(ListNode* pos, LTDataType x);

//删除pos位置
extern void ListErase(ListNode* pos);