#define _CRT_SECURE_NO_WARNINGS
#include "Queue.h"

//初始化
void QueueInit(Queue** ppq)
{
	assert(ppq != NULL);
	*ppq = (Queue*)malloc(sizeof(Queue));
	if (*ppq == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	(*ppq)->head = NULL;
	(*ppq)->tail = NULL;
}

//销毁
void QueueDestroy(Queue** ppq)
{
	assert(ppq != NULL);

	QueueNode* cur = (*ppq)->head;
	while (cur != NULL)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}

	free(*ppq);
	*ppq = NULL;
}

//入队列
void QueuePush(Queue** ppq, QDataType x)
{
	assert(ppq != NULL);

	QueueNode* newnode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newnode == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	
	if ((*ppq)->head == NULL)
	{
		(*ppq)->head = newnode;
		(*ppq)->tail = newnode;
	}
	else
	{
		(*ppq)->tail->next = newnode;
		(*ppq)->tail = (*ppq)->tail->next;
	}
}

//出队列
void QueuePop(Queue** ppq)
{
	assert(ppq != NULL);
	
	if (QueueEmpty(*ppq))
	{
		return;
	}

	if ((*ppq)->head == (*ppq)->tail)
	{
		free((*ppq)->head);
		(*ppq)->head = NULL;
		(*ppq)->tail = NULL;
	}
	else
	{
		QueueNode* next = (*ppq)->head->next;
		free((*ppq)->head);
		(*ppq)->head = next;
	}
}

//获取队头元素
QDataType QueueFront(Queue* pq)
{
	assert(pq != NULL);
	assert(!QueueEmpty(pq));

	return pq->head->data;
}

//获取队尾元素
QDataType QueueBack(Queue* pq)
{
	assert(pq != NULL);
	assert(!QueueEmpty(pq));

	return pq->tail->data;
}

//判空
_Bool QueueEmpty(Queue* pq)
{
	return pq->head == NULL;
}

//获取队列长度
size_t QueueSize(Queue* pq)
{
	size_t count = 0;
	QueueNode* cur = pq->head;
	while (cur != NULL)
	{
		count++;
		cur = cur->next;
	}
	return count;
}