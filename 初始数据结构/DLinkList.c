#define _CRT_SECURE_NO_WARNINGS
#include "DLinkList.h"

//初始化
ListNode* ListInit(void)
{
	ListNode* guard = (ListNode*)malloc(sizeof(ListNode));
	if (guard == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	guard->next = guard;
	guard->prev = guard;

	return guard;
}

//打印 Print
void ListPrint(ListNode* plist)
{
	assert(plist != NULL);

	ListNode* cur = plist->next;
	while (cur != plist)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//动态申请结点 Buy ListNode
ListNode* BuyListNode(LTDataType x)
{
	ListNode* node = (ListNode*)malloc(sizeof(ListNode));
	if (node == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	node->next = NULL;
	node->prev = NULL;
	node->data = x;

	return node;
}

//判空
_Bool ListEmpty(ListNode* plist)
{
	assert(plist != NULL);

	return plist->next == plist;
}

//获取链表的长度
size_t ListSize(ListNode* plist)
{
	assert(plist != NULL);

	size_t count = 0;
	ListNode* cur = plist->next;
	while (cur != plist)
	{
		count++;
		cur = cur->next;
	}
	return count;
}

//尾插
void ListPushBack(ListNode* plist, LTDataType x)
{
	assert(plist != NULL);

	ListNode* newnode = BuyListNode(x);

	newnode->next = plist;
	newnode->prev = plist->prev;
	plist->prev->next = newnode;
	plist->prev = newnode;
}

//头插
void ListPushFront(ListNode* plist, LTDataType x)
{
	assert(plist != NULL);

	ListNode* newnode = BuyListNode(x);

	newnode->next = plist->next;
	newnode->prev = plist;
	plist->next->prev = newnode;
	plist->next = newnode;
}

//尾删
void ListPopBack(ListNode* plist)
{
	assert(plist != NULL);

	if (ListEmpty(plist))
	{
		return;
	}

	ListNode* node = plist->prev;

	node->prev->next = plist;
	plist->prev = node->prev;
	free(node);
}

//头删
void ListPopFront(ListNode* plist)
{
	assert(plist != NULL);

	if (ListEmpty(plist))
	{
		return;
	}

	ListNode* node = plist->next;

	plist->next = node->next;
	node->next->prev = plist;
	free(node);
}

//查找
ListNode* ListFind(ListNode* plist, LTDataType x)
{
	assert(plist != NULL);

	ListNode* cur = plist->next;
	while (cur != plist)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

//在pos之前插入
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos != NULL);
	
	ListNode* newnode = BuyListNode(x);

	newnode->next = pos;
	newnode->prev = pos->prev;
	pos->prev->next = newnode;
	pos->prev = newnode;
}

//删除pos位置
void ListErase(ListNode* pos)
{
	assert(pos != NULL);

	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
}

//销毁
void ListDetroy(ListNode* plist)
{
	assert(plist != NULL);

	ListNode* cur = plist->next;
	ListNode* next = NULL;
	while (cur != plist)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	free(cur);
}