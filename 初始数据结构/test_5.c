#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "LinkedList.h"

typedef SListNode ListNode;

//ListNode* partition(ListNode* pHead, int x) {
//    if (pHead == NULL || pHead->next == NULL)
//    {
//        return pHead;
//    }
//
//    ListNode temp = { 0 };
//    temp.next = pHead;
//
//    ListNode* slow = &temp;
//    ListNode* pre = &temp;
//    ListNode* fast = pHead;
//    ListNode* fastnext = pHead;
//
//    while (fast != NULL)
//    {
//        fastnext = fast->next;
//        if (fast->data < x)
//        {
//            //连接3条线
//            if (pre != slow)
//            {
//                pre->next = fastnext;
//                fast->next = slow->next;
//                slow->next = fast;
//            }
//            else
//            {
//                pre = pre->next;
//            }
//
//            //定位slow、fast、pre(不动)、fastnext在fast不为空时递进
//            slow = slow->next;
//        }
//        else
//        {
//            pre = pre->next;
//        }
//        fast = fastnext;
//    }
//
//    pHead = temp.next;
//    return pHead;
//}

ListNode* partition(ListNode* pHead, int x) {
    ListNode* cur = pHead;

    ListNode h1 = { 0 };
    ListNode h2 = { 0 };
    ListNode* p1 = &h1;
    ListNode* p2 = &h2;

    while (cur != NULL)
    {
        if (cur->data < x)
        {
            p1->next = cur;
            p1 = p1->next;
        }
        else
        {
            p2->next = cur;
            p2 = p2->next;
        }
        cur = cur->next;
    }
    p2->next = NULL;
    p1->next = h2.next;
    pHead = h1.next;
    return pHead;
}

int test_5(void)
{
    ListNode* pHead = NULL;
    SListPushBack(&pHead, 3);
    SListPushBack(&pHead, 6);
    SListPushBack(&pHead, 8);
    SListPushBack(&pHead, 2);
    SListPushBack(&pHead, 4);
    SListPushBack(&pHead, 9);
    SListPushBack(&pHead, 7);
    SListPushBack(&pHead, 1);

    SListPrint(pHead);

    pHead = partition(pHead, 5);

    SListPrint(pHead);

    SListDetroy(&pHead);

    return 0;
}