#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int* merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    int i = m - 1;
    int j = n - 1;
    int k = m + n - 1;
    while (i >= 0 && j >= 0)
    {
        if (nums1[i] > nums2[j])
        {
            nums1[k] = nums1[i];
            i--;
        }
        else
        {
            nums1[k] = nums2[j];
            j--;
        }
        k--;
    }

    while (j >= 0)
    {
        nums1[k] = nums2[j];
        k--;
        j--;
    }

    return nums1;
}


int test_3()
{
    int num1[5+8] = { 1,3,5,7,9 };
    int num2[] = { 2,4,6,8,10,11,12,13 };
    int m = 5;
    int n = 8;

    int* arr = merge(num1, m+n, m, num2, n+n, n);

    int i = 0;
    for ( i = 0; i < m+n; i++)
    {
        printf("%d ", arr[i]);
    }

	return 0;
}