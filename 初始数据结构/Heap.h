#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int HPDataType;

typedef struct Heap
{
	HPDataType* data;
	int size;
	int capacity;
} Heap;

//初始化
extern void HeapInit(Heap** pphp);

//销毁
extern void HeapDestroy(Heap** pphp);

//插入堆
extern void HeapPush(Heap* php, HPDataType x);

//删除堆顶元素
extern void HeapPop(Heap* php);

//获取堆顶元素
extern HPDataType HeapTop(Heap* php);

//判空
extern _Bool HeapEmpty(Heap* php);

//获取堆元素个数
extern size_t HeapSize(Heap* php);

void Swap(HPDataType* a, HPDataType* b);

extern void heapsort(int* arr, int n);

void AdjustDown(HPDataType* data, int size, int parent);