#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"

//初始化
void SeqListInit(SeqList* psl)
{
	assert(psl != NULL);

	psl->data = NULL;	//初始化时不分配空间，在插入时分配空间，不够了就重新申请relloc
	psl->size = 0;
	psl->capacity = 0;
}

//销毁
void SeqListDestroy(SeqList* psl)
{
	assert(psl != NULL);	//psl-data不用判空，free传入NULL是不做处理的

	free(psl->data);
	psl->data = NULL;
	psl->size = 0;
	psl->capacity = 0;
}

//检查容量
void CheckCapacity(SeqList* psl)
{
	assert(psl != NULL);

	if (psl->size == psl->capacity)
	{
		int newCapacity = psl->capacity == 0 ? 4 : 2 * psl->capacity;
		SLDataType* temp = (SLDataType*)realloc(psl->data, newCapacity * sizeof(SLDataType));
		if (temp == NULL)
		{
			perror("realloc fail\n");
			exit(-1);
		}
		psl->data = temp;
		psl->capacity = newCapacity;
	}
}

//尾插 Push Back
void SeqListPushBack(SeqList* psl, SLDataType x)
{
	assert(psl != NULL);

	CheckCapacity(psl);

	psl->data[psl->size] = x;
	psl->size++;
}

//尾删 Pop Back
void SeqListPopBack(SeqList* psl)
{
	assert(psl != NULL);

	if (psl->size == 0)
	{
		return;
	}

	psl->size--;
}

//头插 Push Front
void SeqListPushFront(SeqList* psl, SLDataType x)
{
	assert(psl != NULL);

	CheckCapacity(psl);

	int end = psl->size - 1;
	while (end >= 0)
	{
		psl->data[end + 1] = psl->data[end];
		--end;
	}

	psl->data[0] = x;
	psl->size++;
}

//头删 Pop Front
void SeqListPopFront(SeqList* psl)
{
	assert(psl != NULL);

	if (psl->size == 0)
	{
		return;
	}

	int begin = 0;
	while (begin < psl->size-1)
	{
		psl->data[begin] = psl->data[begin + 1];
		++begin;
	}

	psl->size--;
}

//打印 Print
void SeqListPrint(const SeqList* const psl)
{
	assert(psl != NULL);

	int i = 0;
	for (i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->data[i]);
	}
	printf("\n");
}

//查找 Find
int SeqListFind(SeqList* psl, SLDataType x)
{
	assert(psl != NULL);

	int i = 0;
	for (i = 0; i < psl->size; i++)
	{
		if (psl->data[i] == x)
		{
			return i;
		}
	}

	return -1;
}

//插入 Insert
void SeqListInsert(SeqList* psl, size_t pos, SLDataType x)
{
	assert(psl != NULL);
	assert(pos <= (size_t)psl->size);	//可以等于size，就是尾插

	CheckCapacity(psl);

	//size_t end = psl->size - 1;
	//while (end >= pos)		//如果pos为0，则end=0时执行循环体，end--，end为无符号整型值，--后为整型最大值无限循环，若是将end声明为int型，end与无符号pos比较依然按照无符号来所以是死循环
	//{
	//	psl->data[end + 1] = psl->data[end];
	//	--end;
	//}

	size_t end = psl->size;		//从size开始，end>pos时执行，就不会出现这种情况了
	while (end > pos)
	{
		psl->data[end] = psl->data[end-1];
		--end;
	}

	psl->data[pos] = x;
	psl->size++;
}

//删除 Erase
void SeqListErase(SeqList* psl, size_t pos)
{
	assert(psl != NULL);
	assert(pos < (size_t)psl->size);

	size_t begin = pos;
	while (begin < (size_t)(psl->size - 1))
	{
		psl->data[begin] = psl->data[begin + 1];
		begin++;
	}

	psl->size--;
}

//修改 Modify
void SeqListModify(SeqList* psl, size_t pos, SLDataType x)
{
	assert(psl != NULL);
	assert(pos < (size_t)psl->size);

	psl->data[pos] = x;
}