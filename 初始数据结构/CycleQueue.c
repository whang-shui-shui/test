#define _CRT_SECURE_NO_WARNINGS
#include "CycleQueue.h"

//初始化
void CQueueInit(CQueue** ppq)
{
	assert(ppq != NULL);

	*ppq = (CQueue*)malloc(sizeof(CQueue));
	if (*ppq == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	(*ppq)->data = (CQDataType*)malloc(MAX_DATA * sizeof(CQDataType));
	if ((*ppq)->data == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	(*ppq)->head = (*ppq)->rear = 0;
	(*ppq)->capacity = MAX_DATA;
}

//销毁
void CQueueDestroy(CQueue** ppq)
{
	assert(ppq != NULL);

	free((*ppq)->data);
	free(*ppq);
	*ppq = NULL;
}

//入队列
void CQueuePush(CQueue* pq, CQDataType x)
{
	assert(pq != NULL);

	if (CQueueFull(pq))
	{
		perror("Cycle Queue is Full!\n");
		return;
	}

	pq->data[pq->rear] = x;
	pq->rear = (pq->rear + 1) % pq->capacity;
}

//出队列
void CQueuePop(CQueue* pq)
{
	assert(pq != NULL);

	if (CQueueEmpty(pq))
	{
		return;
	}

	pq->head = (pq->head + 1) % pq->capacity;
}

//判空
_Bool CQueueEmpty(CQueue* pq)
{
	assert(pq != NULL);
	return pq->head == pq->rear;
}

//判满
_Bool CQueueFull(CQueue* pq)
{
	assert(pq != NULL);

	return (pq->rear + 1) % pq->capacity == pq->head;
}

//获取元素个数
size_t CQueueSize(CQueue* pq)
{
	assert(pq != NULL);

	return (pq->rear + pq->capacity - pq->head) % pq->capacity;
}

//获取队头元素
CQDataType CQueueFront(CQueue* pq)
{
	assert(pq != NULL);
	assert(!CQueueEmpty(pq));

	return pq->data[pq->head];
}

//获取队尾元素
CQDataType CQueueBack(CQueue* pq)
{
	assert(pq != NULL);
	assert(!CQueueEmpty(pq));

	return pq->data[(pq->rear - 1 + pq->capacity) % pq->capacity];
}