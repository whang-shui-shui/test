#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MAX_DATA 10

typedef int CQDataType;

typedef struct CQueue
{
	CQDataType* data;
	int head;
	int rear;
	int capacity;
} CQueue;

//初始化
extern void CQueueInit(CQueue** ppq);

//销毁
extern void CQueueDestroy(CQueue** ppq);

//入队列
extern void CQueuePush(CQueue* pq, CQDataType x);

//出队列
extern void CQueuePop(CQueue* pq);

//判空
extern _Bool CQueueEmpty(CQueue* pq);

//判满
extern _Bool CQueueFull(CQueue* pq);

//获取元素个数
extern size_t CQueueSize(CQueue* pq);

//获取队头元素
extern CQDataType CQueueFront(CQueue* pq);

//获取队尾元素
extern CQDataType CQueueBack(CQueue* pq);
