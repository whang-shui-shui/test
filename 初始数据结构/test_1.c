#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

long long count = 0;

long long Fib(int n)
{
	count++;
	if (n < 3)
	{
		return 1;
	}
	return Fib(n - 1) + Fib(n - 2);
}

int cmp(void* e1, void* e2)
{
	return *(int*)e1 - *(int*)e2;
}

int test_1(void)
{
	printf("%lld ", Fib(40));
	printf("%lld", count);

	return 0;
}