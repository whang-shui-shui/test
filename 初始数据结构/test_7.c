#define _CRT_SECURE_NO_WARNINGS
#include "Stack.h"
#include "Queue.h"
#include "CycleQueue.h"

void QueueTest1(void)
{
	Queue* pq = NULL;
	QueueInit(&pq);
	QueuePush(&pq, 1);
	QueuePush(&pq, 2);
	QueuePush(&pq, 3);
	QueuePush(&pq, 4);
	QueuePush(&pq, 5);

	int a = QueueFront(pq);
	int b = QueueBack(pq);
	int len = (int)QueueSize(pq);
	printf("f = %d\tb = %d\tl = %d\n", a, b, len);

	for (size_t i = 0; i < 4; i++)
	{
		QueuePop(&pq);
		a = QueueFront(pq);
		b = QueueBack(pq);
		len = (int)QueueSize(pq);
		printf("f = %d\tb = %d\tl = %d\n", a, b, len);
	}
	QueuePop(&pq);
	len = (int)QueueSize(pq);
	printf("\t\tl = %d\n", len);
	
	QueueDestroy(&pq);
}

void StackTest1(void)
{
	Stack* ps = NULL;
	StackInit(&ps);

	StackPush(ps, 1);
	StackPush(ps, 2);
	StackPush(ps, 3);
	StackPush(ps, 4);
	StackPush(ps, 5);

	for (size_t i = 0; i < 5; i++)
	{
		printf("%d ", StackTop(ps));
		StackPop(ps);
	}

	StackDestroy(&ps);
}

void CQueueTest1(void)
{
	CQueue* pq = NULL;
	CQueueInit(&pq);

	//����1-9��10error
	for (size_t i = 0; i < 10; i++)
	{
		CQueuePush(pq, i+1);
	}
	

	int a = 0;
	int b = 0;
	int c = 0;

	//����1-8
	for (size_t i = 0; i < 8; i++)
	{
		if (!CQueueEmpty(pq))
		{
			a = CQueueFront(pq);
			b = CQueueBack(pq);
		}
		else
		{
			a = -1;
			b = -1;
		}
		
		c = CQueueSize(pq);
		printf("front = %d\trear = %d\tsize = %d\n", a, b, c);
		CQueuePop(pq);
	}
	
	//����10-17
	for (size_t i = 0; i < 8; i++)
	{
		CQueuePush(pq, i + 10);
	}

	//����9-17
	for (size_t i = 0; i < 9; i++)
	{
		if (!CQueueEmpty(pq))
		{
			a = CQueueFront(pq);
			b = CQueueBack(pq);
		}
		else
		{
			a = -1;
			b = -1;
		}

		c = CQueueSize(pq);
		printf("front = %d\trear = %d\tsize = %d\n", a, b, c);
		CQueuePop(pq);
	}

	CQueueDestroy(&pq);
}

int test_7(void)
{
	//QueueTest1();
	//StackTest1();
	CQueueTest1();

	return 0;
}