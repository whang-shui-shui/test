#define _CRT_SECURE_NO_WARNINGS
#include "LinkedList.h"

void LinkedListTest1(void)
{
	SListNode* plist = NULL;
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);

	SListPrint(plist);
}

void LinkedListTest2(void)
{
	SListNode* plist = NULL;
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);

	SListPrint(plist);

	SListNode* pos = SListFind(plist, 3);

	SListInsert(&plist, pos, 9);
	SListInsert(&plist, pos, 8);

	SListPrint(plist);

	SListErase(&plist, pos);
	//SListErase(&plist, pos->next);

	SListPrint(plist);

	SListDetroy(&plist);
}



int test_4(void)
{
	LinkedListTest2();
	return 0;
}