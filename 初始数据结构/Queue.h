#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int QDataType;

typedef struct QueueNode
{
    QDataType data;		//数据
    struct QueueNode* next;	//指向下一个结点的指针
} QueueNode;

typedef struct Queue
{
    QueueNode* head;
    QueueNode* tail;
} Queue;

//初始化
extern void QueueInit(Queue** ppq);

//销毁
extern void QueueDestroy(Queue** ppq);

//入队列
extern void QueuePush(Queue** ppq, QDataType x);

//出队列
extern void QueuePop(Queue** ppq);

//获取队头元素
extern QDataType QueueFront(Queue* pq);

//获取队尾元素
extern QDataType QueueBack(Queue* pq);

//判空
extern _Bool QueueEmpty(Queue* pq);

//获取队列长度
extern size_t QueueSize(Queue* pq);