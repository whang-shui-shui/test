#define _CRT_SECURE_NO_WARNINGS
#include "Heap.h"

void heapsort(int* arr, int n)
{
	//建堆，以第一个数据为堆进行插入调整	-- O(nlogn)
	int i = 0;
	//for ( i = 1; i < n; i++)
	//{
	//	AdjustUp(arr, i);
	//}

	//向下调整建堆，从倒数第一个叶子节点的根节点开始	-- O(n)
	//写成n-1开始也可以，但是不推荐，因为最后一层占一半，非常耗时
	for (i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(arr, n, i);
	}

	//选数
	i = 1;
	while (i < n)
	{
		Swap(&arr[0], &arr[n - i]);
		AdjustDown(arr, n - i, 0);
		i++;
	}
}
