#define _CRT_SECURE_NO_WARNINGS
#include "game.h"

//菜单
void menu()
{
	printf("******************************\n");
	printf("********    1. play    *******\n");
	printf("********    0. exit    *******\n");
	printf("******************************\n");
}

//初始化棋盘
void Init(char board[ROW][COL], const int row, const int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}

//打印棋盘
void DisplayBoard(char board[ROW][COL], const int row, const int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (j < col - 1)
			{
				printf(" %c |", board[i][j]);
			}
			else
			{
				printf(" %c \n", board[i][j]);
			}
		}

		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				if (j < col - 1)
				{
					printf("---|");
				}
				else
				{
					printf("---\n");
				}
			}	
		}
	}
}

//玩家落子
void PlayMove(char board[ROW][COL], int row, int col)
{
	printf("玩家下棋:>\n");
	int x = 0, y = 0;
	while (1)
	{
		printf("请输入坐标:>");
		scanf("%d %d", &x, &y);
		if ((x >= 1) && (x <= row) && (y >= 1) && (y <= col))
		{
			x--;
			y--;
			if (board[x][y] != ' ')
			{
				printf("坐标被占用\n");
			}
			else
			{
				board[x][y] = '*';
				break;
			}
		}
		else
		{
			printf("坐标输入有误，请重新输入");
		}
	}
	
}

//电脑落子
void ComputerMove(char board[ROW][COL], int row, int col)
{
	printf("电脑下棋>\n");
	int x = 0, y = 0;
	while (1)
	{
		x = rand() % row;
		y = rand() % col;

		if (' ' == board[x][y])
		{
			board[x][y] = '#';
			break;
		}
	}
}

//判断输赢
char IsWin(char board[ROW][COL], int row, int col)
{
	//玩家
	if (XofV(board, row, col, '*') || YofV(board, row, col, '*') || XandYofV(board, row, col, '*'))
	{
		return '*';
	}
	else if (XofV(board, row, col, '#') || YofV(board, row, col, '#') || XandYofV(board, row, col, '#'))
	{
		return '#';
	}
	else if(IsFull(board, row, col))
	{
		return 'q';
	}
	else
	{
		return 'c';
	}
}

//判断是否满
_Bool IsFull(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (' ' == board[i][j])
			{
				return 0;
				break;
			}
		}
	}
	return 1;
}

//行赢
_Bool XofV(char board[ROW][COL], int row, int col, char c)
{
	int count = 0;
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (c == board[i][j])
			{
				count++;
			}
			else
			{
				count = 0;
			}
		}

		if (count >= WIN)
		{
			return 1;
		}
		else
		{
			count = 0;
		}
	}

	return 0;
}

//列赢
_Bool YofV(char board[ROW][COL], int row, int col, char c)
{
	return XofV(board, col, row, c);
}

//对角线赢
_Bool XandYofV(char board[ROW][COL], int row, int col, char c)
{
	int count = 0;
	//正对角
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		int x = i;
		for (j = 0; j < col && x < row; j++, x++)
		{
			if (board[x][j] == c)
			{
				count++;
			}
			else
			{
				count = 0;
			}
			
		}

		if (count >= WIN)
		{
			return 1;
		}
		else
		{
			count = 0;
		}
	}

	count = 0;
	int j = 0;
	for (j = 0; j < col; j++)
	{
		int i = 0;
		int y = j;
		for (i = 0; i < row && y < col; i++, y++)
		{
			if (board[i][y] == c)
			{
				count++;
			}
			else
			{
				count = 0;
			}

		}

		if (count >= WIN)
		{
			return 1;
		}
		else
		{
			count = 0;
		}
	}

	//斜对角
	for (i = 0; i < row; i++)
	{
		int j = 0;
		int x = i;
		for (j = col-1; j >= 0 && x < row; j--, x++)
		{
			if (board[x][j] == c)
			{
				count++;
			}
			else
			{
				count = 0;
			}

		}

		if (count >= WIN)
		{
			return 1;
		}
		else
		{
			count = 0;
		}
	}

	count = 0;
	for (j = 0; j < col; j++)
	{
		int i = 0;
		int y = j;
		for (i = row - 1; i >= 0 && y < col; i--, y++)
		{
			if (board[i][y] == c)
			{
				count++;
			}
			else
			{
				count = 0;
			}

		}

		if (count >= WIN)
		{
			return 1;
		}
		else
		{
			count = 0;
		}
	}

	return 0;
}