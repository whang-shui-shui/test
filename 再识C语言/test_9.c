#define _CRT_SECURE_NO_WARNINGS

int test_9(void)
{
	//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水
	int money = 20;
	int empy = 0;
	int water = 0;
	int count = 0;

	while (money >= 1)
	{
		water = money;
		count += water;
		empy = water + empy;
		money = empy / 2;
		empy = empy % 2;
	}

	printf("%d\n", count);
	return 0;
}