#define _CRT_SECURE_NO_WARNINGS
#include <assert.h>
#include "tool_1.h"

//调整数组使奇数全部都位于偶数前面
void my_odd_even(const int* const arr, const int len)
{
	assert(arr != NULL && len > 0);

	int* left = arr;
	int* right = arr + len - 1;

	while (left < right)
	{
		if (*left % 2 == 1)
		{
			left++;
		}
		else
		{
			int temp = *left;
			*left = *right;
			*right = temp;
			right--;
		}
	}
}

void odd_even(int* const arr, const int len)
{
	assert(arr != NULL && len > 0);

	int left = 0;
	int right = len - 1;

	while (left < right)
	{
		while ((left < right) && arr[left] % 2 == 1)
		{
			left++;
		}

		while ((left < right) && arr[right] % 2 == 0)
		{
			right--;
		}

		if (left < right)
		{
			int temp = arr[left];
			arr[left] = arr[right];
			arr[right] = temp;
			left++;
			right--;
		}
	}
}

int test_11(void)
{
	int arr[20] = { 0 };
	int len = 20;
	int i = 0;
	for (i = 0; i < len; i++)
	{
		scanf("%d", &arr[i]);
	}

	//my_odd_even(arr, len);
	odd_even(arr, len);
	print_arr_int(arr, len);
	return 0;
}