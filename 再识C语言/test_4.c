#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int count_binery_of_1(int n)
{
	int count = 0;
	while (0 != n)
	{
		if (n % 2 == 1)
		{
			count++;
		}
		n /= 2;
	}
	return count;
}

int count_binery_of_2(int n)
{
	int count = 0;
	int i = 0;
	for (i = 0; i < 32; i++)
	{
		if ((n >> i) & 1 == 1)
		{
			count++;
		}
	}
	return count;
}

int count_binery_of_3(int n)
{
	/*
	* 15:1111
	* 14:1110
	* 15&14:1110:14
	* 
	* 14:1110
	* 13:1101
	* 14&13:1100:12
	* 
	* 12:1100
	* 11:1001
	* 12&11:1000:8
	* 
	* 8:1000
	* 7:0111
	* 8&7:0000:0
	*/
	int count = 0;
	while (n)
	{
		n = n & (n - 1);
		count++;
	}
	return count;
}

int count_diffrent_of_1(int a, int b)
{
	int count = 0;
	int i = 0;
	for (i = 0; i < 32; i++)
	{
		//每一位对比
		if (((a >> i) & 1) == ((b >> i) & 1))
		{
			count++;
		}
	}
	return count;
}


int count_diffrent_of_2(int a, int b)
{
	/*异或结果中1为不同的位
	*/
	int ret = a ^ b;
	int count = count_binery_of_3(ret);
	return count;
}

void print_number(int n)
{
	int i = 0;
	//打印奇数位
	for (i = 30; i >= 0; i-=2)
	{
		printf("%d", (n >> i) & 1);
	}
	printf("\n");
	//打印偶数位
	for (i = 31; i > 0; i -= 2)
	{
		printf("%d", (n >> i) & 1);
	}
}
/*
* n & (n-1) 可以判断n是否为2的次方
*/

int test_4(void)
{
	int num = 0;
	return 0;
}