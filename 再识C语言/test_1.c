#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void ExchangeByAdd(int* a, int* b);
void ExchangeByDivOr(int* a, int* b);


//不创建临时变量，交换两个数
int test_1(void)
{
	int a = 3;
	int b = 5;
	ExchangeByAdd(&a, &b);
	printf("%d %d\n", a, b);
	a = 3;
	b = 5;
	ExchangeByDivOr(&a, &b);
	printf("%d %d\n", a, b);
}

void ExchangeByAdd(int* a, int* b)
{
	*a = *a + *b;
	*b = *a - *b;
	*a = *a - *b;
}

void ExchangeByDivOr(int* a, int* b)
{
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}