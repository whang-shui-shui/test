#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

//结构体
struct Peo
{
	char name[20];
	int age;
	char sex[5];
};

struct Stu
{
	struct Peo p;
	int num;
	float f;
};

//参数传值
void print_struct_of_frome(struct Stu s)
{
	printf("%s %s %d %d %f\n", s.p.name, s.p.sex, s.p.age, s.num, s.f);
}

//参数传址
void print_struct_of_truth(struct Stu *s)
{
	printf("%s %s %d %d %f\n", s->p.name, s->p.sex, s->p.age, s->num, s->f);
}

int test_3(void)
{
	struct Stu p1 = { {"zhangsan", 20, "man"}, 1, 80.0f};	//初始化
	print_struct_of_frome(p1);
	print_struct_of_truth(&p1);
	return 0;
}