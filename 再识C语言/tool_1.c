#define _CRT_SECURE_NO_WARNINGS
#include "tool_1.h"

void print_arr_int(const int* const arr, const int len)
{
	assert(arr != NULL);
	int i = 0;
	for (i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}