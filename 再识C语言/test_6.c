#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

//��ӡˮ�ɻ���
int number_water(int a)
{
	int n = 0;
	if (0 <= a && a <= 9)
	{
		return a;
	}
	else if (10 <= a && a <= 99)
	{
		n = 2;
	}
	else if (100 <= a && a <= 999)
	{
		n = 3;
	}
	else if (1000 <= a && a <= 9999)
	{
		n = 4;
	}
	else if (10000 <= a && a <= 99999)
	{
		n = 5;
	}
	else
	{
		n = 6;
	}

	int sum = 0;
	while (a > 0)
	{
		sum += (int)pow(a % 10, n);
		a /= 10;
	}
	return sum;
}

int test_6(void)
{
	int i = 0;
	for (i = 0; i <= 100000; i++)
	{
		if (number_water(i) == i)
		{
			printf("%d ", i);
		}
	}
	return 0;
}