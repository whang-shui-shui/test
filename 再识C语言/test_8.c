#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <assert.h>

//写得好的求字符串长度
int my_strlen(const char* str)
{
	assert(str != NULL);
	int count = 0;
	while (*str != '\0')
	{
		str++;
		count++;
	}
	return count;
}

int test_8(void)
{
	char str[] = "12345678";
	printf("%d\n", my_strlen(str));
	return 0;
}