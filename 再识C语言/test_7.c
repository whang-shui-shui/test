#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int test_7(void)
{
	int n = 18;

	int i = 0;
	for (i = 0; i < 2*n; i++)
	{
		int j = 0;
		for (j = 0; j < 2*n; j++)
		{
			//c是以n为中心的偏移量，小于n-1时偏移量是行数，大于n-1时是(n-1) - (i-(n-1))，以下为化简开的式子
			int c = i < n ? i : 2 * (n - 1) - i;

			if ((j <= n + c) && (j >= n - c))
			{
				printf("*");
			}
			else
			{
				printf(" ");
			}
		}
		printf("\n");
	}
}