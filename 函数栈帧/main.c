#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int Add(int x, int y)
{
	return x + y;
}

int main()
{
	int a = 3;
	int b = 5;
	int ret = Add(3, 5);
	printf("%d", ret);
	return 0;
}