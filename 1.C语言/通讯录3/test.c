#define _CRT_SECURE_NO_WARNINGS
#include "contact.h"

int main()
{
	Contact con;
	init(&con);
	//test_input(&con);

	menu();

	int chose = 0;
	do
	{
		printf("请输入您要进行的操作序号>");

		scanf("%d", &chose);

		system("cls");
		menu();

		switch (chose)
		{
		case 0:
			printf("欢迎下次光临！\n");
			saveContact(&con);
			destroy(&con);
			return 0;
			break;

		case 1:
			add(&con);
			break;

		case 2:
			del(&con);
			break;

		case 3:
			search(&con);
			break;

		case 4:
			alter(&con);
			break;

		case 5:
			show(&con);
			break;

		case 6:
			sort(&con);
			break;

		default:
			printf("输入错误，请重新输入\n");
			break;
		}

	} while (0 != chose);

	return 0;
}