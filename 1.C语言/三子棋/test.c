#define _CRT_SECURE_NO_WARNINGS
#include "game.h"

//游戏
void game()
{
	//创建棋盘
	char board[ROW][COL] = { 0 };
	char win = ' ';

	Init(board, ROW, COL);
	DisplayBoard(board, ROW, COL);

	//双方对局
	while (1)
	{
		PlayMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		win = IsWin(board, ROW, COL);
		if (win != 'c')
		{
			break;
		}
		
		system("cls");
		ComputerMoveAndPrint(board, ROW, COL);
		win = IsWin(board, ROW, COL);
		if (win != 'c')
		{
			break;
		}
	}
	
	//判定结果
	system("cls");
	DisplayBoard(board, ROW, COL);
	if ('*' == win)
	{
		printf("玩家胜利\n");
	}
	else if ('#' == win)
	{
		printf("电脑胜利\n");
	}
	else
	{
		printf("平局\n");
	}
}

//开始
int main(void)
{
	int input = 0;
	srand(time(NULL));
	do
	{
		menu();
		printf("请操作>");
		scanf("%d", &input);

		switch (input)
		{
		case 1:
			system("cls");
			game();
			break;

		case 0:
			printf("欢迎下次光临~\n");
			break;

		default:
			printf("数字输入错误\n");
			break;
		}

	} while (0 != input);

	return 0;
}