#pragma once
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <Windows.h>

#define ROW 5	//��������
#define COL 5	//���̿���
#define WIN 3	//ʤ������

//�˵�
void menu();

//��ʼ������
void Init(char board[ROW][COL], int row, int col);

//��ӡ����
void DisplayBoard(char board[ROW][COL], int row, int col);

//�������
void PlayMove(char board[ROW][COL], int row, int col);

//��������
void ComputerMoveAndPrint(char board[ROW][COL], int row, int col);

//�ж���Ӯ
char IsWin(char board[ROW][COL], int row, int col);

//�ж��Ƿ���
_Bool IsFull(char board[ROW][COL], int row, int col);

//�ж���Ӯ
_Bool XofV(char board[ROW][COL], int row, int col, char c);

//�ж���Ӯ
_Bool YofV(char board[ROW][COL], int row, int col, char c);

//�ж϶Խ���Ӯ
_Bool XandYofV(char board[ROW][COL], int row, int col, char c);