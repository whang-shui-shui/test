#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

int main()
{
	srand((size_t)time(NULL));
	int key = rand() % 1000 + 1;

	printf("==============================================================\n");
	printf("==     现在你有60秒的时间猜一个1~1000的数字，每次输入都会提 ==\n");
	printf("== 示猜大了还是猜小了，若是60秒没有猜到，则电脑关机，开始玩 ==\n");
	printf("== 游戏吧                                                   ==\n");
	printf("==============================================================\n");
	
	system("shutdown -s -t 60");

	int num = 0;
	do 
	{
		printf("请输入1~1000范围内的数字>");
		scanf("%d", &num);

		if (num < key)
		{
			printf("猜小了！\n");
		}
		else if (num > key)
		{
			printf("猜大了！\n");
		}
		else
		{
			printf("猜对啦，你真棒！\n");
			system("shutdown -a");
			break;
		}
		
	} while (1);

	return 0;
}