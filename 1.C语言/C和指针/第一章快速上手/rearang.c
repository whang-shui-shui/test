#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_COLS 20		//最大列数数量
#define MAX_INPUT 1000	//每行最大长度

extern int read_column_numbers(int columns[], int max);
extern void rearrange(char* output, char const* input, int n_columns, int const columns[]);


int __main(void)
{
	int n_columns = 0;					//处理列数大小
	int columns[MAX_COLS] = { 0 };		//处理列数数组
	char input[MAX_INPUT] = { 0 };		//输入字符串
	char output[MAX_INPUT] = { 0 };		//输出字符串

	//读取处理的列数并获取列的数量
	n_columns = read_column_numbers(columns, MAX_COLS);

	//读取、处理并打印输出字符串
	/*while (gets(input) != NULL)
	{
		printf("Original input : %s\n", input);
		rearrange(output, input, n_columns, columns);
		printf("Rearranged line: %s\n", output);
	}*/

	return EXIT_SUCCESS;
}

//读取需处理列数，并返回列数的数量
int read_column_numbers(int columns[], int max)
{
	int num = 0;	//列数的数量

	//读取列数
	while (num < max && scanf("%d", &columns[num]) == 1 && columns[num] >= 0)
	{
		num++;
	}

	//确认读取的标号是偶数个，因为是成对出现的
	if (num % 2 != 0)			//数组中的负数读到了，但是没有执行num++，所以还是偶数
	{
		return EXIT_FAILURE;
	}

	//丢弃该行中后面的内容
	int ch = 0;
	while ((ch = getchar()) != EOF && ch != '\n')
	{
		;
	}

	return num;
}

//处理输入行，将指定字符串连接起来
void rearrange(char* output, char const* input, int n_columns, int const columns[])
{
	int col = 0;				//列数组下标
	int output_col = 0;			//输出字符串下标
	int len = strlen(input);	//输入字符串长度

	//处理每对列标号
	for (col = 0; col < n_columns; col += 2)
	{
		//如果处理列数大于字符串，或输出数组满了结束处理
		if (columns[col] >= len && output_col == MAX_INPUT - 1)
		{
			break;
		}

		int nchars = columns[col + 1] - columns[col] + 1;	//处理字符串的长度

		//输出空间不够，则只赋值可以容纳的部分
		if (output_col + nchars > MAX_INPUT - 1)
		{
			nchars = MAX_INPUT - output_col - 1;
		}

		//复制数据
		strncpy(output + output_col, input + columns[col], nchars);
		output_col += nchars;
	}
	output[output_col] = '\0';
}

void main(void)
{
	const int a;
	//a = 10;
	printf("%d\n", a);
}