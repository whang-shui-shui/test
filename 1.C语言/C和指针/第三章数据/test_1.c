#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

static int w = 5;
extern int x;		//任何地方调用都错误，未定义标识符

static float
func1(int a, int b, int c)
{
	//int c, d, e = 1;	c重定义
	int d, e = 1;
	{
		int d, e, w;
		{
			int b, c, d;
			static int y = 2;
		}
	}
	{
		register int a, d, x;
		extern int y;//后面调用报错，未定义标识符
	}
}

static int y;

float
func2(int a)
{
	extern int y;
	static int z;
}

int main()
{
	func1(1, 2, 3);
	func2(2);
	return 0;
}