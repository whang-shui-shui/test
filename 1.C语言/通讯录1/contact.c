#define _CRT_SECURE_NO_WARNINGS
#include "contact.h"

// 菜单
void menu()
{
	printf("========================================\n");
	printf("==------------------------------------==\n");
	printf("==------  1.add       2.del     ------==\n");
	printf("==------  3.search    4.alter   ------==\n");
	printf("==------  5.show      6.sort    ------==\n");
	printf("==------        0.exst          ------==\n");
	printf("==------------------------------------==\n");
	printf("========================================\n");
}

//初始化
void init(Contact* pc)
{
	assert(pc != NULL);

	pc->count = 0;
	memset(pc, 0, sizeof(pc));	//将Contact内存设置为0

	////假装加载，显得有逼格
	//printf("正在加载通讯录......\n");
	//Sleep(5000);
	//printf("加载完毕，开始使用吧！！！\n");
}

//获取指定姓名的联系人下标
int getIndexByName(Contact* pc, const char* name)
{
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		if (strcmp(pc->data[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}

// 添加
void add(Contact* pc)
{
	assert(pc != NULL);
	if (pc->count >= MAX_INFO)
	{
		printf("通讯录已满\n");
		return;
	}

	printf("请输入姓名>");
	scanf("%s", pc->data[pc->count].name);

	printf("请输入性别>");
	scanf("%s", pc->data[pc->count].sex);

	printf("请输入年龄>");
	scanf("%d", &(pc->data[pc->count].age));

	printf("请输入电话>");
	scanf("%s", pc->data[pc->count].tel);

	printf("请输入地址>");
	scanf("%s", pc->data[pc->count].addr);

	pc->count++;

	printf("添加成功\n");
}

// 删除
void del(Contact* pc)
{
	assert(pc != NULL);

	if (pc->count == 0)
	{
		printf("通讯录为空\n");
		return;
	}

	char name[20] = { 0 };

	printf("请输入删除信息的姓名>");
	scanf("%s", name);

	int i = getIndexByName(pc, name);

	if (i != -1)
	{
		for (i; i < pc->count - 1; i++)
		{
			memcpy(&(pc->data[i]), &(pc->data[i + 1]), sizeof(PeoInfo));
		}
		pc->count--;
		printf("删除成功\n");
	}
	else
	{
		printf("查无此人\n");
	}
}

// 修改
void alter(Contact* pc)
{
	assert(pc != NULL);
	if (pc->count == 0)
	{
		printf("通讯录为空\n");
		return;
	}

	char name[20] = { 0 };
	printf("请输入被修改联系人的姓名>");
	scanf("%s", name);

	int i = getIndexByName(pc, name);

	if (i != -1)
	{
		printf("请输入姓名>");
		scanf("%s", pc->data[i].name);

		printf("请输入性别>");
		scanf("%s", pc->data[i].sex);

		printf("请输入年龄>");
		scanf("%d", &(pc->data[i].age));

		printf("请输入电话>");
		scanf("%s", pc->data[i].tel);

		printf("请输入地址>");
		scanf("%s", pc->data[i].addr);

		printf("修改成功\n");
	}
	else
	{
		printf("查无此人\n");
	}
}

// 查寻
void search(const Contact* pc)
{
	assert(pc != NULL);

	if (pc->count == 0)
	{
		printf("通讯录为空\n");
		return;
	}

	char name[20] = { 0 };
	printf("请输入姓名>");
	scanf("%s", name);

	int i = getIndexByName(pc, name);
	if (i != -1)
	{
		printf("%-20s\t%-10s\t%-10s\t%-20s\t%-20s\n",
			"姓名", "性别", "年龄", "电话", "地址");
		printf("%-20s\t%-10s\t%-10d\t%-20s\t%-20s\n",
			pc->data[i].name, pc->data[i].sex, pc->data[i].age, pc->data[i].tel, pc->data[i].addr);
	}
	else
	{
		printf("查无此人\n");
	}
}

// 展示
void show(const Contact* pc)
{
	assert(pc != NULL);

	printf("%-20s\t%-10s\t%-10s\t%-20s\t%-20s\n",
		"姓名", "性别", "年龄", "电话", "地址");
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		printf("%-20s\t%-10s\t%-10d\t%-20s\t%-20s\n",
			pc->data[i].name, pc->data[i].sex, pc->data[i].age, pc->data[i].tel, pc->data[i].addr);
	}
}

//比较方式
int cmp_info(const void* e1, const void* e2)
{
	return strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}
 
// 排序
void sort(Contact* pc)
{
	assert(pc != NULL);

	if (pc->count == 0)
	{
		printf("通讯录为空\n");
		return;
	}

	qsort(pc->data, pc->count, sizeof(PeoInfo), cmp_info);
	printf("排序成功\n");
}

void test_input(Contact* pc)
{
	assert(pc != NULL);
	//printf("测试数据已插入\n");

	strcpy(pc->data[0].name, "zhangsan");
	strcpy(pc->data[1].name, "lisi");
	strcpy(pc->data[2].name, "wangwu");
	strcpy(pc->data[3].name, "zhaoliu");
	strcpy(pc->data[4].name, "yangqi");

	strcpy(pc->data[0].sex, "nan");
	strcpy(pc->data[1].sex, "nv");
	strcpy(pc->data[2].sex, "nan");
	strcpy(pc->data[3].sex, "nv");
	strcpy(pc->data[4].sex, "nan");

	pc->data[0].age = 15;
	pc->data[1].age = 15;
	pc->data[2].age = 15;
	pc->data[3].age = 15;
	pc->data[4].age = 15;

	strcpy(pc->data[0].tel, "15487921455");
	strcpy(pc->data[1].tel, "17892545658");
	strcpy(pc->data[2].tel, "15189456425");
	strcpy(pc->data[3].tel, "16587898465");
	strcpy(pc->data[4].tel, "13954845645");

	strcpy(pc->data[0].addr, "zhangjiabao");
	strcpy(pc->data[1].addr, "lijiacun");
	strcpy(pc->data[2].addr, "wangjiahe");
	strcpy(pc->data[3].addr, "zhaojiaxiang");
	strcpy(pc->data[4].addr, "yangjiadang");

	pc->count += 5;
}