#pragma once
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <Windows.h>

//变量最大数量
#define MAX_INFO 100	//联系人
#define MAX_NAME 20		//姓名
#define MAX_SEX 10		//性别
#define MAX_TEL 20		//电话
#define MAX_ADDR 50		//地址

//个人信息
typedef struct PeoInfo
{
	char name[MAX_NAME];	//姓名
	char sex[MAX_SEX];		//性别
	int age;				//年龄
	char tel[MAX_TEL];		//电话
	char addr[MAX_ADDR];	//地址
} PeoInfo;

//通讯录
typedef struct Contact
{
	PeoInfo data[MAX_INFO];	//联系人
	int count;				//联系人数量
} Contact;


// 菜单
void menu();

//初始化
void init(Contact* pc);

// 添加
void add(Contact* pc);

// 删除
void del(Contact* pc);

// 查寻
void search(const Contact* pc);

// 修改
void alter(Contact* pc);

// 排序
void sort(Contact* pc);

// 展示
void show(const Contact* pc);

//插入测试数据
void test_input(Contact* pc);