#define _CRT_SECURE_NO_WARNINGS
#include "DLinkList.h"

void DLinkListTest1(void)
{
	ListNode* plist = ListInit();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushFront(plist, 3);
	ListPushFront(plist, 4);
	ListPushFront(plist, 5);

	ListPrint(plist);

	ListPopBack(plist);
	ListPrint(plist);
	ListPopBack(plist);
	ListPrint(plist);
	ListPopBack(plist);
	ListPrint(plist);
	ListPopBack(plist);
	ListPrint(plist);
	ListPopBack(plist);
	ListPrint(plist);
	ListPopBack(plist);
	ListPrint(plist);
}

void DLinkListTest2(void)
{
	ListNode* plist = ListInit();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);

	ListPrint(plist);

	ListNode* pos = ListFind(plist, 3);

	ListInsert(pos, 9);
	ListInsert(pos, 8);
	ListInsert(pos, 7);

	ListPrint(plist);

	ListErase(pos->prev);
	ListErase(pos->prev);
	ListErase(pos->prev);

	ListPrint(plist);

	ListDetroy(plist);
	plist = NULL;
}

int test_6(void)
{
	DLinkListTest2();
	return 0;
}