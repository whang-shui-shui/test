#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "SeqList.h"

void SeqListTest1()
{
	SeqList s = { 0 };
	SeqListInit(&s);

	SeqListPushBack(&s, 1);
	SeqListPushBack(&s, 2);

	SeqListPushFront(&s, 11);
	SeqListPushFront(&s, 12);

	SeqListPrint(&s);

	SeqListPopBack(&s);
	SeqListPrint(&s);
	SeqListPopBack(&s);
	SeqListPrint(&s);
	SeqListPopBack(&s);
	SeqListPrint(&s);
	SeqListPopBack(&s);
	SeqListPrint(&s);
	SeqListPopBack(&s);
	SeqListPrint(&s);

	SeqListDestroy(&s);
}

void SeqListTest2()
{
	SeqList s = { 0 };
	SeqListInit(&s);

	SeqListPushBack(&s, 1);
	SeqListPushBack(&s, 2);
	SeqListPushBack(&s, 3);
	SeqListPushBack(&s, 4);
	SeqListPushBack(&s, 5);

	SeqListInsert(&s, 3, 0);
	SeqListInsert(&s, 3, 9);
	SeqListInsert(&s, 3, 8);

	SeqListPrint(&s);

	SeqListErase(&s, 3);
	SeqListPrint(&s);
	SeqListErase(&s, 3);
	SeqListPrint(&s);
	SeqListErase(&s, 3);
	SeqListPrint(&s);

	SeqListDestroy(&s);
}
int test_2()
{
	SeqListTest2();
	return 0;
}