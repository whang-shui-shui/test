#define _CRT_SECURE_NO_WARNINGS
#include "Heap.h"
#include <time.h>

//设置文件数据
extern void SetFILE(void);

//TopK函数
void PrintTopK(const char* filename, int k)
{
	FILE* pf = fopen(filename, "r");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}

	int* minHeap = (int*)malloc(sizeof(int) * k);
	if (minHeap == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	//读取前k个数
	int i = 0;
	for (i = 0; i < k; i++)
	{
		fscanf(pf, "%d ", &minHeap[i]);
	}

	//建堆
	for (i = (k - 2) / 2; i >= 0; i--)
	{
		AdjustDown(minHeap, k, i);
	}

	//读取k到N个数
	int data = 0;
	while (fscanf(pf, "%d ", &data) != EOF)
	{
		if (minHeap[0] < data)
		{
			minHeap[0] = data;
			AdjustDown(minHeap, k, 0);
		}
	}
	
	//打印
	for (i = 0; i < k; i++)
	{
		printf("%d ", minHeap[i]);
	}

	free(minHeap);
	fclose(pf);
}

int test_9(void)
{
	SetFILE();
	char* filename = "整数数据.txt";
	PrintTopK(filename, 10);
	return 0;
}

//设置文件数据
void SetFILE(void)
{
	FILE* pf = fopen("整数数据.txt", "w");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}

	srand((size_t)time(NULL));

	int i = 0;
	for (i = 0; i < 100000; i++)
	{
		fprintf(pf, "%d ", i);
	}


	fclose(pf);
}