#define _CRT_SECURE_NO_WARNINGS
#include "Stack.h"

//初始化
void StackInit(Stack** pps)
{
	assert(pps != NULL);
	*pps = (Stack*)malloc(sizeof(Stack));
	if (*pps == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	(*pps)->data = NULL;
	(*pps)->capacity = 0;
	(*pps)->top = 0;
}

//销毁
void StackDestroy(Stack** pps)
{
	assert(pps != NULL);
	free((*pps)->data);
	free(*pps);
	*pps = NULL;
}

//入栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps != NULL);
	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* temp = (STDataType*)realloc(ps->data, newcapacity * sizeof(STDataType));
		if (temp == NULL)
		{
			perror("realloc fail\n");
			exit(-1);
		}
		ps->capacity = newcapacity;
		ps->data = temp;
	}
	ps->data[ps->top] = x;
	ps->top++;
}


//出栈
void StackPop(Stack* ps)
{
	assert(ps != NULL);

	if (ps->top == 0)
	{
		return;
	}

	ps->top--;
}


//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps != NULL);
	assert(ps->top != 0);
	return ps->data[ps->top - 1];
}

//判空
_Bool StackEmpty(Stack* ps)
{
	assert(ps != NULL);
	return ps->top == 0;
}

//获取栈元素个数
size_t StackSize(Stack* ps)
{
	return (size_t)(ps->top);
}