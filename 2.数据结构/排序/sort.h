#pragma once
#include <stdio.h>

extern void PrintArray(int* arr, int size);


extern void InsertSort(int* arr, int size);

extern void ShellSort(int* arr, int size);

extern void SelectSort(int* arr, int size);

extern void BubbleSort(int* arr, int size);

extern void QuickSort(int* arr, int begin, int end);

extern void QuickSortNonR(int* arr, int begin, int end);

extern void MergeSort(int* arr, int size);

extern void MergeSortNonR(int* arr, int size);