#define _CRT_SECURE_NO_WARNINGS
#include "sort.h"
#include <stdlib.h>
#include <time.h>

void SetArray(char* filename, int size);
void InputArray(char* filename, int* arr, int size);
void OutputArray(char* filename, int* arr, int size);

void test_1(void)
{
	//初始化数组
	int sizeNums = 100000;
	int* nums = (int*)malloc(sizeNums * sizeof(int));
	if (nums == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	//读取文件
	char* filename = "array.txt";
	SetArray(filename, sizeNums);		//设置文件
	InputArray(filename, nums, sizeNums);
	
	//排序并计时
	time_t begin = time(NULL);

	//排序算法：

	//InsertSort(nums, sizeNums);
	//ShellSort(nums, sizeNums);
	//SelectSort(nums, sizeNums);
	//BubbleSort(nums, sizeNums);
	//QuickSort(nums, 0, sizeNums - 1);
	//QuickSortNonR(nums, 0, sizeNums - 1);
	//MergeSort(nums, sizeNums);
	MergeSortNonR(nums, sizeNums);
	
	//打印时间
	time_t end = time(NULL);
	printf("Time: %lld\n", end - begin);

	//回写文件
	OutputArray(filename, nums, sizeNums);

	free(nums);
	nums = NULL;
}

void InputArray(char* filename, int* arr, int size)
{
	FILE* pf = fopen(filename, "r");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}

	int i = 0;
	for (i = 0; i < size; i++)
	{
		fscanf(pf, "%d ", &arr[i]);
		if (arr[i] < 0)
		{
			printf("fail ftell: %d，i=%d\n", ftell(pf), i);
			printf("prevent array value: %d", arr[i-1]);
			exit(-1);
		}
	}
	fclose(pf);
}

void OutputArray(char* filename, int* arr, int size)
{
	FILE* pf = fopen(filename, "w");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}

	int i = 0;
	for (i = 0; i < size; i++)
	{
		fprintf(pf, "%d ", arr[i]);
		if (i != 0 && i % 100 == 0)
		{
			fprintf(pf, "\n");
		}
	}
	fclose(pf);
}

void SetArray(char* filename, int size)
{
	srand((size_t)time(NULL));

	FILE* pf = fopen(filename, "w");
	if (pf == NULL)
	{
		perror("fopen fail");
		exit(-1);
	}

	int i = 0;
	for (i = 0; i < size; i++)
	{
		fprintf(pf, "%d ", rand() % (size * 10));
		if (i != 0 && i % 100 == 0)
		{
			fprintf(pf, "\n");
		}
	}
	fclose(pf);
}