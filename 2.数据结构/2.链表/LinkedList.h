#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int SLTDataType;

typedef struct SListNode
{
    SLTDataType data;		//数据
    struct SListNode* next;	//指向下一个结点的指针
} SListNode;

//动态申请结点 Buy SListNode
extern SListNode* BuySListNode(SLTDataType x);

//打印 Print
extern void SListPrint(SListNode* plist);

//尾插
extern void SListPushBack(SListNode** pplist, SLTDataType x);

//头插
extern void SListPushFront(SListNode** pplist, SLTDataType x);

//尾删
extern void SListPopBack(SListNode** pplist);

//头删
extern void SListPopFront(SListNode** pplist);

//销毁
extern void SListDetroy(SListNode** pplist);

//查找
extern SListNode* SListFind(SListNode* plist, SLTDataType x);

//在pos之前插入
extern void SListInsert(SListNode** pplist, SListNode* pos, SLTDataType x);

//删除pos位置
extern void SListErase(SListNode** pplist, SListNode* pos);