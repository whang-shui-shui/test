#define _CRT_SECURE_NO_WARNINGS
#include "LinkedList.h"

//打印 Print
void SListPrint(SListNode* plist)
{
	//plist不能断言，链表为空时，可以打印，只是没有数值，不能终止程序
	SListNode* cur = plist;
	while (cur != NULL)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//动态申请结点 Buy SListNode
SListNode* BuySListNode(SLTDataType x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));
	if (newnode == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

//头插
void SListPushFront(SListNode** pplist, SLTDataType x)
{
	assert(pplist != NULL);

	SListNode* newnode = BuySListNode(x);

	newnode->next = *pplist;
	*pplist = newnode;
}

//尾插
void SListPushBack(SListNode** pplist, SLTDataType x)
{
	assert(pplist != NULL);

	SListNode* newnode = BuySListNode(x);
	
	if (*pplist == NULL)
	{
		*pplist = newnode;
	}
	else
	{
		SListNode* cur = *pplist;
		while (cur->next != NULL)
		{
			cur = cur->next;
		}

		cur->next = newnode;
	}
}

//头删
void SListPopFront(SListNode** pplist)
{
	assert(pplist != NULL);

	if (*pplist == NULL)
	{
		return;
	}

	SListNode* cur = *pplist;
	*pplist = (*pplist)->next;
	free(cur);
	cur = NULL;
}

//尾删
void SListPopBack(SListNode** pplist)
{
	assert(pplist != NULL);

	if (*pplist == NULL)
	{
		return;
	}

	if ((*pplist)->next == NULL)	//只有一个结点
	{
		free(*pplist);
		*pplist = NULL;
	}
	else	//有多个结点
	{
		SListNode* cur = *pplist;
		while (cur->next->next != NULL)
		{
			cur = cur->next;
		}
		free(cur->next);
		cur->next = NULL;
	}
}

//销毁
void SListDetroy(SListNode** pplist)
{
	assert(pplist != NULL);

	SListNode* cur = *pplist;
	while (cur != NULL)
	{
		*pplist = cur->next;
		free(cur);
		cur = *pplist;
	}
}

//查找
SListNode* SListFind(SListNode* plist, SLTDataType x)
{
	SListNode* cur = plist;
	while (cur != NULL)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

//在pos之前插入
void SListInsert(SListNode** pplist, SListNode* pos, SLTDataType x)
{
	assert(pplist != NULL && pos != NULL);

	if (*pplist == pos)			//pos为头结点时，作为头插，直接调用函数
	{
		SListPushFront(pplist, x);
	}
	else
	{
		SListNode* cur = *pplist;
		while (cur->next != pos)
		{
			cur = cur->next;
			assert(cur != NULL);	//pos不在链表中
		}

		SListNode* newnode = BuySListNode(x);
		newnode->next = pos;
		cur->next = newnode;
	}
}

//删除pos位置
void SListErase(SListNode** pplist, SListNode* pos)
{
	assert(pplist != NULL && pos != NULL);

	if (*pplist == NULL)
	{
		return;
	}

	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		SListNode* cur = *pplist;
		while (cur->next != pos)
		{
			cur = cur->next;
			assert(cur != NULL);
		}

		cur->next = pos->next;
		free(pos);
		pos = NULL;
	}
}