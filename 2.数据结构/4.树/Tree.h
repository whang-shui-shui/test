#pragma once

typedef int TDataType;

//孩子兄弟表示法
typedef struct Tree
{
	TDataType data;
	struct Tree* child;
	struct Tree* brother;
};

//双亲表示法