#define _CRT_SECURE_NO_WARNINGS
#include "Heap.h"

//初始化
void HeapInit(Heap** pphp)
{
	assert(pphp != NULL);

	*pphp = (Heap*)malloc(sizeof(Heap));
	if (*pphp == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}
	(*pphp)->data = NULL;
	(*pphp)->capacity = (*pphp)->size = 0;
}

//销毁
void HeapDestroy(Heap** pphp)
{
	assert(pphp != NULL);

	free((*pphp)->data);
	free(*pphp);
	*pphp = NULL;
}

//交换数据
void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType temp = *a;
	*a = *b;
	*b = temp;
}

//堆向上调整
void AdjustUp(HPDataType* data, int child)
{
	int parent = 0;
	while (child > 0)
	{
		parent = (child - 1) / 2;
		if (data[child] < data[parent])
		{
			Swap(&data[child], &data[parent]);
			child = parent;
		}
		else
		{
			break;
		}
	}
}

//向下调整
void AdjustDown(HPDataType* data, int size, int parent)
{
	int minchild = parent * 2 + 1;
	while (minchild < size)
	{
		//左孩子存在，右孩子未必存在，所以要判断一下child+1
		if (minchild + 1 < size && data[minchild] > data[minchild + 1])
		{
			minchild++;
		}

		if (data[minchild] < data[parent])
		{
			Swap(&data[minchild], &data[parent]);
			parent = minchild;
			minchild = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


//插入堆
void HeapPush(Heap* php, HPDataType x)
{
	assert(php != NULL);

	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* temp = (HPDataType*)realloc(php->data, newcapacity * sizeof(HPDataType));
		if (temp == NULL)
		{
			perror("malloc fail\n");
			exit(-1);
		}
		php->data = temp;
		php->capacity = newcapacity;
	}

	php->data[php->size] = x;
	php->size++;

	AdjustUp(php->data, php->size - 1);
}

//删除堆顶元素
void HeapPop(Heap* php)
{
	assert(php != NULL);

	if (php->size == 0)
	{
		return;
	}

	php->size--;
	Swap(&(php->data[0]), &(php->data[php->size]));

	AdjustDown(php->data, php->size, 0);
}

//获取堆顶元素
HPDataType HeapTop(Heap* php)
{
	assert(php != NULL);
	assert(php->size != 0);

	return php->data[0];
}

//判空
_Bool HeapEmpty(Heap* php)
{
	assert(php != NULL);
	return php->size == 0;
}

//获取堆元素个数
size_t HeapSize(Heap* php)
{
	assert(php != NULL);
	return (size_t)php->size;
}