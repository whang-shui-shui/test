#pragma once
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

typedef int BTDataType;
typedef struct BTreeNode
{
	BTDataType data;
	struct BTreeNode* left;
	struct BTreeNode* right;
}BTNode;

extern void PreOrder(BTNode* root);
extern void InOrder(BTNode* root);
extern void PostOrder(BTNode* root);