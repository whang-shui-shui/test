#define _CRT_SECURE_NO_WARNINGS
#include "BTree.h"

//创建树
BTNode* BTreeCreate(BTNode* root, BTDataType* px)
{
	if (*px == )
}

//先序遍历
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

//中序遍历
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

//后序遍历
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}

	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//节点个数
int BTreeSize(BTNode* root)
{
	return root == NULL ? 0 : BTreeSize(root->left) + BTreeSize(root->right) + 1;
}

//叶子节点个数
int BTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}

	return BTreeSize(root->left) + BTreeSize(root->right);
}

//树深度
int BTreeHeight(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}

	int left = BTreeHeight(root->left);
	int right = BTreeHeight(root->right);
	return (left > right ? left : right) + 1;
}

//第k层节点个数
int BTreeKLevel(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}

	if (k == 1)
	{
		return 1;
	}

	return BTreeKLevel(root->left, k - 1) + BTreeKLevel(root->right, k - 1);
}

//返回x所在节点
BTNode* BTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}

	if (root->data == x)
	{
		return root;
	}

	BTNode* left = BTreeFind(root->left, x);
	if (left != NULL)
	{
		return left;
	}

	BTNode* right = BTreeFind(root->right, x);
	if(right != NULL)
	{
		return right;
	}

	return NULL;

}