#define _CRT_SECURE_NO_WARNINGS
#include "Heap.h"
#include "main.h"

void HeapTest1(void)
{
	Heap* php = NULL;
	HeapInit(&php);

	for (int i = 10; i > 0; i--)
	{
		HeapPush(php, i);
	}

	for (int i = 0; i < 10; i++)
	{
		printf("%d ", HeapTop(php));
		HeapPop(php);
	}

	HeapDestroy(&php);
}

void HeapSortTest(void)
{
	int arr[] = { 2,3,5,7,2,5,8,54,32,754,27 };
	int len = sizeof(arr) / sizeof(arr[0]);
	heapsort(arr, len);

	for (size_t i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
}

int test_8(void)
{
	//HeapTest1();
	HeapSortTest();
	return 0;
}