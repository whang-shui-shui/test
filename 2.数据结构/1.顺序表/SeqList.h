#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define SL_DATA_NUMBER 100


typedef int SLDataType;

typedef struct SeqList
{
	SLDataType* data;
	int size;
	int capacity;
} SeqList;


//��ʼ�� Init
extern void SeqListInit(SeqList* psl);

//������� Check Capacity
extern void CheckCapacity(SeqList* psl);

//β�� Push Back
extern void SeqListPushBack(SeqList* psl, SLDataType x);

//βɾ Pop Back
extern void SeqListPopBack(SeqList* psl);

//ͷ�� Push Front
extern void SeqListPushFront(SeqList* psl, SLDataType x);

//ͷɾ Pop Front
extern void SeqListPopFront(SeqList* psl);

//���� Find
extern int SeqListFind(SeqList* psl, SLDataType x);

//���� Insert
extern void SeqListInsert(SeqList* psl, size_t pos, SLDataType x);

//ɾ�� Erase
extern void SeqListErase(SeqList* psl, size_t pos);

//�޸� Modify
extern void SeqListModify(SeqList* psl, size_t pos, SLDataType x);

//���� Destory
extern void SeqListDestroy(SeqList* psl);

//��ӡ Print
extern void SeqListPrint(const SeqList* const psl);