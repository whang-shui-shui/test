#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define COUNT 10

//初始化
void InitBoard(char board[ROWS][COLS], int rows, int cols, char c);

//部署地雷
void SetMine(char mine[ROWS][COLS], int row, int col, int count);

//打印
void DisplayBoard(char board[ROWS][COLS], int row, int col);

//排查地雷
_Bool FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col);

//获取该子周围的雷的数量
int get_mine_count(char mine[ROWS][COLS], int row, int col, int x, int y);
