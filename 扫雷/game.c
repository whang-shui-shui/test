#define _CRT_SECURE_NO_WARNINGS
#include "game.h"

//初始化
void InitBoard(char board[ROWS][COLS], int rows, int cols, char c)
{
	int i = 0;
	for (i = 0; i < rows; i++)
	{
		int j = 0;
		for (j = 0; j < cols; j++)
		{
			board[i][j] = c;
		}
	}
}

//部署地雷
void SetMine(char mine[ROWS][COLS], int row, int col, int count)
{
	while (count > 0)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;

		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}

//打印
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int k = (col - 5) / 2;
	int i = 0;

	printf("-");
	for (i = 0; i <= col; i++)
	{
		if (i < k || i >= 5+k)
		{
			printf("--");
		}
		else
		{
			printf("扫雷小游戏");
			i += 5;
		}
		
	}
	printf("\n");

	for (i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		int j = 0;
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}

//排查地雷
_Bool FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int num = 0;	//已排查的数量
	do
	{
		printf("请输入坐标>");
		scanf("%d %d", &x, &y);

		//判断坐标是否非法
		if ((0 < x && x <= row) && (0 < y && y <= col))
		{
			//判断坐标是否重复
			if (show[x][y] == '*')
			{
				//此处是雷
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了\n");
					DisplayBoard(mine, row, col);
					break;

				}
				//此处不是雷
				else
				{
					num++;
					int count = get_mine_count(mine, row, col, x, y);
					show[x][y] = (char)count + '0';
					system("cls");
					DisplayBoard(show, row, col);
				}
			}
			else
			{
				printf("该位置已被排查\n");
			}
		}
		else
		{
			printf("坐标输入错误\n");
		}

		//判断输赢
		if (num >= row * col - COUNT)
		{
			printf("恭喜你，你赢了\n");
			DisplayBoard(mine, row, col);
			break;
		}
	} while (1);

	return 0;
}

int get_mine_count(char mine[ROWS][COLS], int row, int col, int x, int y)
{
	return (mine[x - 1][y - 1]
		+ mine[x - 1][y]
		+ mine[x - 1][y + 1]
		+ mine[x][y - 1]
		+ mine[x][y + 1]
		+ mine[x + 1][y - 1]
		+ mine[x + 1][y]
		+ mine[x + 1][y + 1])
		- (8 * '0');
}